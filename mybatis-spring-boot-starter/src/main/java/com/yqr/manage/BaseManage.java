package com.yqr.manage;

import java.util.List;

import com.yqr.mapper.MyMapper;
import org.apache.ibatis.session.RowBounds;


public abstract class BaseManage<T>  {
    
    protected abstract MyMapper<T> getMapper();
    
    public int insertBatch(List<T> recordList) {
        return getMapper().insertBatch(recordList);
    }
    
    
    public int updateBatch(List<T> recordList) {
        return getMapper().insertBatch(recordList);
    }
    
    
    public int deleteByPrimaryKey(Object o) {
        return getMapper().deleteByPrimaryKey(o);
    }
    
    
    public int delete(T t) {
        return getMapper().delete(t);
    }
    
    
    public int insert(T t) {
        return getMapper().insert(t);
    }
    
    
    public int insertSelective(T t) {
        return getMapper().insertSelective(t);
    }
    
    
    public boolean existsWithPrimaryKey(Object o) {
        return getMapper().existsWithPrimaryKey(o);
    }
    
    
    public List<T> selectAll() {
        return getMapper().selectAll();
    }
    
    
    public T selectByPrimaryKey(Object o) {
        return getMapper().selectByPrimaryKey(o);
    }
    
    
    public int selectCount(T t) {
        return getMapper().selectCount(t);
    }
    
    
    public List<T> select(T t) {
        return getMapper().select(t);
    }
    
    
    public T selectOne(T t) {
        return getMapper().selectOne(t);
    }
    
    
    public int updateByPrimaryKey(T t) {
        return getMapper().updateByPrimaryKey(t);
    }
    
    
    public int updateByPrimaryKeySelective(T t) {
        return getMapper().updateByPrimaryKeySelective(t);
    }
    
    
    public int deleteByExample(Object o) {
        return getMapper().deleteByExample(o);
    }
    
    
    public List<T> selectByExample(Object o) {
        return getMapper().selectByExample(o);
    }
    
    
    public int selectCountByExample(Object o) {
        return getMapper().selectCountByExample(o);
    }
    
    
    public T selectOneByExample(Object o) {
        return getMapper().selectOneByExample(o);
    }
    
    
    public int updateByExample(T t, Object o) {
        return getMapper().updateByExample(t,o);
    }
    
    
    public int updateByExampleSelective(T t, Object o) {
        return getMapper().updateByExampleSelective(t,o);
    }
    
    
    public List<T> selectByExampleAndRowBounds(Object o, RowBounds rowBounds) {
        return getMapper().selectByExampleAndRowBounds(o,rowBounds);
    }
    
    
    public List<T> selectByRowBounds(T t, RowBounds rowBounds) {
        return getMapper().selectByRowBounds(t,rowBounds);
    }
    
    
    public int insertList(List<? extends T> list) {
        return getMapper().insertList(list);
    }
    
    
    public int insertUseGeneratedKeys(T t) {
        return getMapper().insertUseGeneratedKeys(t);
    }
}
