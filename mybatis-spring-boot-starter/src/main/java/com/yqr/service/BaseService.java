package com.yqr.service;

import com.github.pagehelper.PageInfo;

import java.util.List;

public interface BaseService<T> {
    /**
     * 根据id查询记录
     * @param id
     * @return
     */
    T selectById(Long id);
    
    /**
     * 查询所有的记录
     * @return
     */
    List<T> selectAll();
    
    /**
     * 根据条件查询单条记录
     * @param record
     * @return
     */
    T selectOne(T record);
    
    /**
     * 根据条件查询多条记录
     * @param record
     * @return
     */
    List<T> selectListByWhere(T record);
    
    /**
     * 根据条件分页查询多条记录
     * @param record
     * @param page
     * @param rows
     * @return
     */
    PageInfo<T> selectPageListByWhere(T record, Integer page, Integer rows);
    
    /**
     * 添加所有字段
     * @param record
     * @return
     */
    Integer add(T record);
    
    /**
     * 更新
     * @param record
     * @return
     */
    Integer updateSelective(T record);
    
    /**
     * 根据id删除
     * @param id
     * @return
     */
    Integer deleteById(Integer id);
    
    /**
     *
     * @param clazz
     * @param preporty
     * @param values
     * @return
     */
    Integer deleteByPreportys(Class<T> clazz, String preporty, Object[] values);
    
    /**
     * 根据条件删除记录
     * @param record
     * @return
     */
    Integer deleteByWhere(T record);
}
