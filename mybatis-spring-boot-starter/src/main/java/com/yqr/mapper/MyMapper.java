package com.yqr.mapper;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;

/**
 * 继承自己的 通用MyMapper
 */
@tk.mybatis.mapper.annotation.RegisterMapper
public interface MyMapper<T> extends Mapper<T>, MySqlMapper<T> {

    /**
     * 批量插入（所有字段） 与 insertList的不同
     * 非自增自定义ID场景使用
     *
     * @param recordList
     * @return
     */
    @org.apache.ibatis.annotations.InsertProvider(type = InsertBatchProvider.class, method = "dynamicSQL")
    int insertBatch(List<T> recordList);

    /**
     * 批量插入（所有字段） 与 insertBatch的不同
     *
     * @param recordList
     * @return
     */
    @org.apache.ibatis.annotations.UpdateProvider(type = InsertBatchProvider.class, method = "dynamicSQL")
    int updateBatch(List<T> recordList);

    /**
     * 乐观锁控制更新，更新数据数量为0，提示异常
     *
     * @param t
     * @return
     */
    default boolean updateByPrimaryKeyWithVersion(T t) {
        int result = updateByPrimaryKey(t);
        if (result == 0) {
            throw new RuntimeException("更新失败");
        }
        return result == 1;
    }
}
