package com.yqr.common.config;

import com.yqr.common.properties.IdWorkerProp;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.sohu.idcenter.IdWorker;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 默认配置
 */
@Configuration
@Slf4j
@Data
public class DefaultCfg {

    private static final Long SEQUENCE=1L;
    private static final Long IDEPOCH=1618383505000L;
    
    @Bean
    @ConfigurationProperties(prefix = "id-worker")
    public IdWorkerProp idWorkerProp() {
        return new IdWorkerProp();
    }
    
    @Bean("idWorker")
    public IdWorker getIdWorker(IdWorkerProp idWorkerProp) {
        IdWorker idWorker=new IdWorker(idWorkerProp.getWorkerId(),idWorkerProp.getDatacenterId(),SEQUENCE,IDEPOCH);
        return idWorker;
    }
}
