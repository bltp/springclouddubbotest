package com.yqr.common.config;

import javax.servlet.http.HttpServletRequest;

import com.yqr.common.enums.BaseEnumFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * @author Simon
 * 将请求参数转换成自定义枚举类型
 */
@Configuration
@ConditionalOnClass({HttpServletRequest.class})
public class MyEnumConvertConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new BaseEnumFactory());
        WebMvcConfigurer.super.addFormatters(registry);
    }
}
