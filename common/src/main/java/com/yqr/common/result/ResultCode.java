package com.yqr.common.result;

import lombok.AllArgsConstructor;
import lombok.Getter;
 /**
     * @Description: 响应返回状态码
     * @author Mr.Qian
     * @date 2021/3/15 11:28
     */
@Getter
@AllArgsConstructor
public enum ResultCode {
    /**
     * 成功业务响应
     */
    OK(200, "操作成功"),
    /**
     * 系统异常
     */
    SYSTEM_ERROR(400, "系统异常"),
    /**
     * 默认业务异常
     */
    ERROR(500, "业务异常"),

    /**
     * 会话失效
     */
    SESSION_INVALID(600, "会话失效"),
    /**
     * 库存锁定
     */
    BILL_LOCKED(501, "库存锁定"),
    /**
     * 库存盘点
     */
    BILL_TAKING(502, "库存盘点"),
    /**
     * 鉴权失败
     */
    API_AUTH_ERROR(601, "鉴权失败"),
    ;

    /**
     * code编码
     */
    final int code;
    /**
     * 描述
     */
    final String message;


}
