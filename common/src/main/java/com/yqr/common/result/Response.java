package com.yqr.common.result;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;


 /**
     * @Description: 返回信息
     * @author Mr.Qian
     * @date 2021/3/15 11:29
     */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Response<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private T response;
    private long time = System.currentTimeMillis();
}
