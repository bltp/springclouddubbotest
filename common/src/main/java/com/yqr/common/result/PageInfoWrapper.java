package com.yqr.common.result;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

 /**
     * @Description: 分页响应结构
     * @author Mr.Qian
     * @date 2021/3/15 11:29
     */
@Data
public class PageInfoWrapper<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("分页信息")
    private PageInfo<T> pageInfo;

    @ApiModelProperty("分页数据")
    private Collection<T> list;

    public PageInfoWrapper() {
    }

    public PageInfoWrapper(PageInfo<T> pageInfo, Collection<T> list) {
        pageInfo.setList(null);
        this.pageInfo = pageInfo;
        this.list = list;
    }

    public PageInfoWrapper(PageInfo<T> pageInfo) {
        this.list = pageInfo.getList();
        pageInfo.setList(null);
        this.pageInfo = pageInfo;
    }

    public static <T> PageInfoWrapper<T> build(PageInfo<T> pageInfo) {
        return new PageInfoWrapper<>(pageInfo);
    }

    public static <T> PageInfoWrapper<T> build(PageInfo<T> pageInfo, Collection<T> list) {
        return new PageInfoWrapper<>(pageInfo, list);
    }

    public void setPageInfo(PageInfo<T> pageInfo) {
        this.list = pageInfo.getList();
        pageInfo.setList(null);
        this.pageInfo = pageInfo;
    }
}
