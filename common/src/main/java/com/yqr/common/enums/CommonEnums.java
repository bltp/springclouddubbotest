package com.yqr.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class CommonEnums {
    @AllArgsConstructor
    public enum Gender implements IBaseEnum{
        Female("男"),Male("女");
        @Getter
        private String desc;
    }
    @AllArgsConstructor
    public enum YesOrNo implements IBaseEnum{
        Y("yes"),N("no");
        @Getter
        private String desc;
    }
}
