package com.yqr.common.exception;
 /**
     * @Description: 业务异常
     * @author Mr.Qian
     * @date 2021/3/15 11:30
     */
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = 2221019601835886746L;
    /**
     * 异常情况的分类，由业务方定义
     */
    private int errorType;

    public ServiceException() {
        super();
    }

    public ServiceException(String s) {
        super(s);
    }

    public ServiceException(int errorType, String s) {
        super(s);
        this.errorType = errorType;
    }

    public int getErrorType() {
        return errorType;
    }

    public void setErrorType(int errorType) {
        this.errorType = errorType;
    }

    public ServiceException(Throwable e) {
        super(e);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
