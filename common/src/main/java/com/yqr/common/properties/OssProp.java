package com.yqr.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * OSS 配置类，从commons读取配置信息给oss项目使用
 *
 * @author tim
 * @date 2021/4/17 20:28
 */
@Data
@ConfigurationProperties(prefix = OssProp.PREFIX)
public class OssProp {

    public static final String PREFIX = "oss";
    /**
     * 目前只有"cn-hangzhou"这个region可用, 不要使用填写其他region的值
     */
    private String regionCn;
    /**
     * 只有 RAM用户（子账号）才能调用 AssumeRole 接口
     * 阿里云主账号的AccessKeys不能用于发起AssumeRole请求
     * 请首先在RAM控制台创建一个RAM用户，并为这个用户创建AccessKeys
     */
    private  String stsAccessKeyId;
    private  String stsAccessKeySecret;
    /**
     * RoleArn 需要在 RAM 控制台上获取
     */
    private  String stsRoleArn;
    private  String aliyunOssBucket;
    /**
     * 过期时间 900s 15分钟
     */
    private  Long stsDurationSeconds;
    private  String endpoint;
    private  String internalEndpoint;
}
