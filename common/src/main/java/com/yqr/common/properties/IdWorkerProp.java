package com.yqr.common.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 全局default config
 *
 * @author xubang
 */
@Data
public class IdWorkerProp {
    private Long datacenterId;
    private Long workerId;
}
