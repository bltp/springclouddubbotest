package com.yqr.server1;

public interface OrderService {
    /**
     * 创建订单
     */
    void create(String userId, String commodityCode, int orderCount);
}
