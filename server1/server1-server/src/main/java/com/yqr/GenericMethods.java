package com.yqr;

import java.io.Serializable;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class GenericMethods extends ObjectContainer<String> {
    
    public GenericMethods(String contained) {
        super(contained);
    }
    
    @Override
    public void setContained(String contained) {
        super.setContained(contained);
    }
    
    public  <T> T getObject(T t){
        return t;
    }
    public <E extends Integer> E get(E e){
        return e;
    }
    public static void main(String[] args) {
    
        GenericMethods genericMethods=new GenericMethods("sdf");
        ObjectContainer objectContainer=(ObjectContainer)genericMethods;
        objectContainer.setContained(12);
        
    }
}
class ObjectContainer<T> {
    private T contained;
    public ObjectContainer(T contained) {
        this.contained = contained;
    }
    public T getContained() {
        return contained;
    }
    public void setContained(T contained) {
        this.contained = contained;
    }
}
