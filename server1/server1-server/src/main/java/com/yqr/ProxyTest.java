package com.yqr;

import com.yqr.server1.TestProxy;
import com.yqr.server1.TestProxyImpl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @program: springclouddubbotest
 * @description: 测试动态代理
 * @author: Mr.Qian
 * @create: 2021-03-31 14:29
 **/
public class ProxyTest implements InvocationHandler {
    private Object testProxy;
    public Object bind(TestProxy testProxy){
        this.testProxy=testProxy;
        return Proxy.newProxyInstance(testProxy.getClass().getClassLoader(),testProxy.getClass().getInterfaces(),this);
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("welcome");
        method.invoke(testProxy,args);
        return "";
    }
    
    public static void main(String[] args) {
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles","true");
        TestProxy testProxy=(TestProxy)new ProxyTest().bind(new TestProxyImpl());
        testProxy.rInt();
    }
}
