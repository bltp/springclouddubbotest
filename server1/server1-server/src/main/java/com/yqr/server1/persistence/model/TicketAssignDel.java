package com.yqr.server1.persistence.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import lombok.ToString;

@ToString
@ApiModel("")
@Table(name = "ticket_assign_del")
public class TicketAssignDel {
    @ApiModelProperty("")
    private Long id;

    /**
     * 关联的简版公司id
     */
    @Column(name = "company_id")
    @ApiModelProperty("关联的简版公司id")
    private Long companyId;

    /**
     * 修改人
     */
    @Column(name = "update_by")
    @ApiModelProperty("修改人")
    private Long updateBy;

    /**
     * 财务id
     */
    @Column(name = "create_by")
    @ApiModelProperty("财务id")
    private Long createBy;

    /**
     * 冗余字段：更新人姓名
     */
    @Column(name = "update_byname")
    @ApiModelProperty("冗余字段：更新人姓名")
    private String updateByname;

    /**
     * 票号
     */
    @Column(name = "ticket_no")
    @ApiModelProperty("票号")
    private String ticketNo;

    /**
     * 票面金额
     */
    @Column(name = "ticket_amount")
    @ApiModelProperty("票面金额")
    private BigDecimal ticketAmount;

    /**
     * 到期日期
     */
    @Column(name = "ticket_terminal_date")
    @ApiModelProperty("到期日期")
    private Date ticketTerminalDate;

    /**
     * 出票日
     */
    @Column(name = "ticket_create_date")
    @ApiModelProperty("出票日")
    private Date ticketCreateDate;

    /**
     * 出票人全称
     */
    @Column(name = "ticket_send_name")
    @ApiModelProperty("出票人全称")
    private String ticketSendName;

    /**
     * 出票人账号
     */
    @Column(name = "ticket_send_no")
    @ApiModelProperty("出票人账号")
    private String ticketSendNo;

    /**
     * 出票人开户银行
     */
    @Column(name = "ticket_send_bank_name")
    @ApiModelProperty("出票人开户银行")
    private String ticketSendBankName;

    /**
     * 收票人全称
     */
    @Column(name = "ticket_receive_name")
    @ApiModelProperty("收票人全称")
    private String ticketReceiveName;

    /**
     * 收票人账号
     */
    @Column(name = "ticket_receive_no")
    @ApiModelProperty("收票人账号")
    private String ticketReceiveNo;

    /**
     * 收票人开户银行
     */
    @Column(name = "ticket_receive_bank_name")
    @ApiModelProperty("收票人开户银行")
    private String ticketReceiveBankName;

    /**
     * 承兑人账号
     */
    @Column(name = "ticket_accept_no")
    @ApiModelProperty("承兑人账号")
    private String ticketAcceptNo;

    /**
     * 承兑人全称
     */
    @Column(name = "ticket_accept_name")
    @ApiModelProperty("承兑人全称")
    private String ticketAcceptName;

    /**
     * 承兑人开户行行号
     */
    @Column(name = "ticket_accept_bank_no")
    @ApiModelProperty("承兑人开户行行号")
    private String ticketAcceptBankNo;

    /**
     * 承兑人开户行名称
     */
    @Column(name = "ticket_accept_bank_name")
    @ApiModelProperty("承兑人开户行名称")
    private String ticketAcceptBankName;

    /**
     * 能否转让
     */
    @Column(name = "ticket_is_sell")
    @ApiModelProperty("能否转让")
    private String ticketIsSell;

    /**
     * 承兑协议编号/交易合同号
     */
    @Column(name = "ticket_deal_no")
    @ApiModelProperty("承兑协议编号/交易合同号")
    private String ticketDealNo;

    /**
     * 出票保证人姓名
     */
    @Column(name = "ticket_send_guarantee")
    @ApiModelProperty("出票保证人姓名")
    private String ticketSendGuarantee;

    /**
     * 承兑保证人姓名
     */
    @Column(name = "ticket_accept_guarantee")
    @ApiModelProperty("承兑保证人姓名")
    private String ticketAcceptGuarantee;

    /**
     * 出票人信用等级
     */
    @Column(name = "ticket_send_credit_level")
    @ApiModelProperty("出票人信用等级")
    private String ticketSendCreditLevel;

    /**
     * 出票人评级到期日
     */
    @Column(name = "ticket_send_credit_terminal_date")
    @ApiModelProperty("出票人评级到期日")
    private String ticketSendCreditTerminalDate;

    /**
     * 承兑人信用等级
     */
    @Column(name = "ticket_accept_credit_level")
    @ApiModelProperty("承兑人信用等级")
    private String ticketAcceptCreditLevel;

    /**
     * 承兑人评级到期日
     */
    @Column(name = "ticket_accept_credit_terminal_date")
    @ApiModelProperty("承兑人评级到期日")
    private String ticketAcceptCreditTerminalDate;

    /**
     * 票面正面图片
     */
    @Column(name = "ticket_front_img_url")
    @ApiModelProperty("票面正面图片")
    private String ticketFrontImgUrl;

    /**
     * 票面被面图片
     */
    @Column(name = "ticket_back_img_url")
    @ApiModelProperty("票面被面图片")
    private String ticketBackImgUrl;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    @ApiModelProperty("更新时间")
    private Date updateTime;

    /**
     * 票据类型：1电银2电商
     */
    @Column(name = "ticket_type")
    @ApiModelProperty("票据类型：1电银2电商")
    private String ticketType;

    /**
     * 银行域名
     */
    @Column(name = "bank_domain")
    @ApiModelProperty("银行域名")
    private String bankDomain;

    /**
     * 票据瑕疵 1绝对记载事项缺失 2不可转让
     */
    @Column(name = "ticket_flaw")
    @ApiModelProperty("票据瑕疵 1绝对记载事项缺失 2不可转让")
    private String ticketFlaw;

    /**
     * 票据状态
     */
    @Column(name = "ticket_state")
    @ApiModelProperty("票据状态")
    private String ticketState;

    /**
     * 检验结果：0正常1正面异常2背面异常3正背面异常
     */
    @Column(name = "check_result")
    @ApiModelProperty("检验结果：0正常1正面异常2背面异常3正背面异常")
    private String checkResult;

    /**
     * 背书次数
     */
    @Column(name = "recite_account")
    @ApiModelProperty("背书次数")
    private Integer reciteAccount;

    /**
     * 承兑方开户行简称
     */
    @Column(name = "ticket_accept_bank_shortname")
    @ApiModelProperty("承兑方开户行简称")
    private String ticketAcceptBankShortname;

    /**
     * 承兑人类别
     */
    @Column(name = "ticket_accept_type")
    @ApiModelProperty("承兑人类别")
    private String ticketAcceptType;

    /**
     * 处理状态1待入库2入库中3待签收4已签收5已拒签6已出库
     */
    @Column(name = "assign_state")
    @ApiModelProperty("处理状态1待入库2入库中3待签收4已签收5已拒签6已出库")
    private String assignState;

    /**
     * 调整天
     */
    @Column(name = "adjustment_days")
    @ApiModelProperty("调整天")
    private Integer adjustmentDays;

    /**
     * 出票人承诺
     */
    @Column(name = "ticket_send_promise")
    @ApiModelProperty("出票人承诺")
    private String ticketSendPromise;

    /**
     * 保证人姓名
     */
    @ApiModelProperty("保证人姓名")
    private String promiser;

    /**
     * 保证人地址
     */
    @Column(name = "promiser_address")
    @ApiModelProperty("保证人地址")
    private String promiserAddress;

    /**
     * 保证日期
     */
    @Column(name = "promiser_date")
    @ApiModelProperty("保证日期")
    private String promiserDate;

    /**
     * 承兑日期
     */
    @Column(name = "ticket_accept_date")
    @ApiModelProperty("承兑日期")
    private String ticketAcceptDate;

    /**
     * 承兑人承诺
     */
    @Column(name = "ticket_accept_promise")
    @ApiModelProperty("承兑人承诺")
    private String ticketAcceptPromise;

    /**
     * 票面金额中文
     */
    @Column(name = "ticket_amount_chinese")
    @ApiModelProperty("票面金额中文")
    private String ticketAmountChinese;

    /**
     * 承兑汇票中文名字
     */
    @Column(name = "ticket_type_name")
    @ApiModelProperty("承兑汇票中文名字")
    private String ticketTypeName;

    /**
     * 提交人id
     */
    @Column(name = "submit_user_id")
    @ApiModelProperty("提交人id")
    private Long submitUserId;

    /**
     * 提交人姓名
     */
    @Column(name = "submit_user_name")
    @ApiModelProperty("提交人姓名")
    private String submitUserName;

    /**
     * 购入员工ID
     */
    @Column(name = "buy_user_id")
    @ApiModelProperty("购入员工ID")
    private Long buyUserId;

    /**
     * 购入员工姓名
     */
    @Column(name = "buy_user_name")
    @ApiModelProperty("购入员工姓名")
    private String buyUserName;

    /**
     * 购入价类型 1直扣 2每十万扣款 3年息 4无
     */
    @Column(name = "buy_type")
    @ApiModelProperty("购入价类型 1直扣 2每十万扣款 3年息 4无")
    private String buyType;

    /**
     * 拟售价类型 1直扣 2每十万扣款 3年息 4无
     */
    @Column(name = "expect_type")
    @ApiModelProperty("拟售价类型 1直扣 2每十万扣款 3年息 4无")
    private String expectType;

    /**
     * 拟购价（年息）
     */
    @Column(name = "buy_point")
    @ApiModelProperty("拟购价（年息）")
    private BigDecimal buyPoint;

    /**
     * 拟售价（年息）
     */
    @Column(name = "expect_point")
    @ApiModelProperty("拟售价（年息）")
    private BigDecimal expectPoint;

    /**
     * 购入价直扣
     */
    @Column(name = "buy_direct_deduction")
    @ApiModelProperty("购入价直扣")
    private BigDecimal buyDirectDeduction;

    /**
     * 购入价每10万扣款
     */
    @Column(name = "buy_lakh_debit")
    @ApiModelProperty("购入价每10万扣款")
    private BigDecimal buyLakhDebit;

    /**
     * 购入价手续费
     */
    @Column(name = "buy_handle_charge")
    @ApiModelProperty("购入价手续费")
    private BigDecimal buyHandleCharge;

    /**
     * 拟售价直扣
     */
    @Column(name = "expect_direct_deduction")
    @ApiModelProperty("拟售价直扣")
    private BigDecimal expectDirectDeduction;

    /**
     * 拟售价每十万扣款
     */
    @Column(name = "expect_lakh_debit")
    @ApiModelProperty("拟售价每十万扣款")
    private BigDecimal expectLakhDebit;

    /**
     * 拟售价手续费
     */
    @Column(name = "expect_handle_charge")
    @ApiModelProperty("拟售价手续费")
    private BigDecimal expectHandleCharge;

    /**
     * 购入数量
     */
    @Column(name = "stock_num")
    @ApiModelProperty("购入数量")
    private Integer stockNum;

    /**
     * 提交入库备注
     */
    @Column(name = "commit_common")
    @ApiModelProperty("提交入库备注")
    private String commitCommon;

    /**
     * 警示
     */
    @Column(name = "ticket_warnings")
    @ApiModelProperty("警示")
    private String ticketWarnings;

    /**
     * 最后一手背书人
     */
    @Column(name = "last_recite_name")
    @ApiModelProperty("最后一手背书人")
    private String lastReciteName;

    /**
     * 当前所在交易户，签收后的最后一手被背书人
     */
    @Column(name = "assgin_endorsee_name")
    @ApiModelProperty("当前所在交易户，签收后的最后一手被背书人")
    private String assginEndorseeName;

    /**
     * 总金额
     */
    @Column(name = "total_amount")
    @ApiModelProperty("总金额")
    private BigDecimal totalAmount;

    @Column(name = "repeat_count")
    @ApiModelProperty("")
    private Integer repeatCount;

    @Column(name = "big_return_count")
    @ApiModelProperty("")
    private Integer bigReturnCount;

    @Column(name = "small_return_count")
    @ApiModelProperty("")
    private Integer smallReturnCount;

    @Column(name = "pledge_count")
    @ApiModelProperty("")
    private Integer pledgeCount;

    @Column(name = "total_page")
    @ApiModelProperty("")
    private Integer totalPage;

    /**
     * 背书人上出现出票人
     */
    @Column(name = "has_ticket_send2")
    @ApiModelProperty("背书人上出现出票人")
    private Integer hasTicketSend2;

    /**
     * 背书人上出现收票人
     */
    @Column(name = "has_ticket_receive2")
    @ApiModelProperty("背书人上出现收票人")
    private Integer hasTicketReceive2;

    /**
     * 签收二次识别开关状态 1:打开 0：关闭
     */
    @Column(name = "second_confirm_flag")
    @ApiModelProperty("签收二次识别开关状态 1:打开 0：关闭")
    private String secondConfirmFlag;

    /**
     * 内部记事
     */
    @Column(name = "internal_commons")
    @ApiModelProperty("内部记事")
    private String internalCommons;

    /**
     * 二次签收正面临时生成的图片
     */
    @Column(name = "second_sign_front_img_url")
    @ApiModelProperty("二次签收正面临时生成的图片")
    private String secondSignFrontImgUrl;

    /**
     * 打码的正面图片
     */
    @Column(name = "ticket_front_mask_img_url")
    @ApiModelProperty("打码的正面图片")
    private String ticketFrontMaskImgUrl;

    /**
     * 期限分类0-不足月 1-半年 2-一年 3-超期
     */
    @Column(name = "deadline_classify")
    @ApiModelProperty("期限分类0-不足月 1-半年 2-一年 3-超期")
    private String deadlineClassify;

    /**
     * "0" :不占资  "1":占资
     */
    @Column(name = "capital_type")
    @ApiModelProperty("\"0\" :不占资  \"1\":占资")
    private String capitalType;

    @Column(name = "ticket_client")
    @ApiModelProperty("")
    private String ticketClient;

    /**
     * 入库时间
     */
    @Column(name = "stock_in_time")
    @ApiModelProperty("入库时间")
    private Date stockInTime;

    /**
     * 出库时间
     */
    @Column(name = "stock_out_time")
    @ApiModelProperty("出库时间")
    private Date stockOutTime;

    /**
     * 识别时是否开启显示警示"0" :未开启  "1":开启
     */
    @Column(name = "show_warning_flag")
    @ApiModelProperty("识别时是否开启显示警示\"0\" :未开启  \"1\":开启")
    private String showWarningFlag;

    @Column(name = "user_has_read")
    @ApiModelProperty("")
    private Boolean userHasRead;

    @Column(name = "batch_md5")
    @ApiModelProperty("")
    private String batchMd5;

    @Column(name = "batch_parent_id")
    @ApiModelProperty("")
    private Long batchParentId;

    @Column(name = "batch_count")
    @ApiModelProperty("")
    private Integer batchCount;

    @Column(name = "catch_mode")
    @ApiModelProperty("")
    private Integer catchMode;

    /**
     * 识别时是否开启显示瑕疵"0" :未开启  "1":开启
     */
    @Column(name = "show_flaw_flag")
    @ApiModelProperty("识别时是否开启显启")
    private String showFlawFlag;

    /**
     * 调度人名字
     */
    @Column(name = "dispatch_byname")
    @ApiModelProperty("调度人名字")
    private String dispatchByname;

    /**
     * 调度人id
     */
    @Column(name = "dispatch_by")
    @ApiModelProperty("调度人id")
    private Long dispatchBy;

    /**
     * 拟售价年息天数
     */
    @Column(name = "expect_point_day")
    @ApiModelProperty("拟售价年息天数")
    private Integer expectPointDay;

    /**
     * 拟购价年息天数
     */
    @Column(name = "buy_point_day")
    @ApiModelProperty("拟购价年息天数")
    private Integer buyPointDay;

    /**
     * 挂票人员工姓名
     */
    @Column(name = "sell_user_name")
    @ApiModelProperty("挂票人员工姓名")
    private String sellUserName;

    /**
     * 挂票人员工id
     */
    @Column(name = "sell_user_id")
    @ApiModelProperty("挂票人员工id")
    private Long sellUserId;

    /**
     * 删除时间
     */
    @Column(name = "delete_time")
    @ApiModelProperty("删除时间")
    private Date deleteTime;

    /**
     * 供应链商票状态0,未认证1,认证中2,已认证4,已出库5,已到期
     */
    @Column(name = "chain_status")
    @ApiModelProperty("供应链商票状态0,未认证1,认证中2,已认证4,已出库5,已到期")
    private String chainStatus;

    /**
     * 数据类型 0-普通 1-链属票据
     */
    @Column(name = "data_type")
    @ApiModelProperty("数据类型 0-普通 1-链属票据")
    private String dataType;

    /**
     * 贸易背景审核状态 1：审核通过 0：待审核  8:未提交 9：审核失败
     */
    @Column(name = "approval_status")
    @ApiModelProperty("贸易背景审核状态 1：审核通过 0：待审核  8:未提交 9：审核失败")
    private String approvalStatus;

    /**
     * 票据操作类型 1承兑2签收
     */
    @Column(name = "ticket_operate_type")
    @ApiModelProperty("票据操作类型 1承兑2签收")
    private String ticketOperateType;

    /**
     * 出票人是否已认证0未认证1已认证
     */
    @Column(name = "drawer_certify_status")
    @ApiModelProperty("出票人是否已认证0未认证1已认证")
    private String drawerCertifyStatus;

    /**
     * 数据操作时间，比如认证，审核，等
     */
    @Column(name = "operate_time")
    @ApiModelProperty("数据操作时间，比如认证，审核，等")
    private Date operateTime;

    /**
     * 上一次抓取时票据状态
     */
    @Column(name = "last_ticket_state")
    @ApiModelProperty("上一次抓取时票据状态")
    private String lastTicketState;

    @Column(name = "batch_bank_domains")
    @ApiModelProperty("")
    private String batchBankDomains;

    @Column(name = "relative_batch_ids")
    @ApiModelProperty("")
    private String relativeBatchIds;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取关联的简版公司id
     *
     * @return company_id - 关联的简版公司id
     */
    public Long getCompanyId() {
        return companyId;
    }

    /**
     * 设置关联的简版公司id
     *
     * @param companyId 关联的简版公司id
     */
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    /**
     * 获取修改人
     *
     * @return update_by - 修改人
     */
    public Long getUpdateBy() {
        return updateBy;
    }

    /**
     * 设置修改人
     *
     * @param updateBy 修改人
     */
    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * 获取财务id
     *
     * @return create_by - 财务id
     */
    public Long getCreateBy() {
        return createBy;
    }

    /**
     * 设置财务id
     *
     * @param createBy 财务id
     */
    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    /**
     * 获取冗余字段：更新人姓名
     *
     * @return update_byname - 冗余字段：更新人姓名
     */
    public String getUpdateByname() {
        return updateByname;
    }

    /**
     * 设置冗余字段：更新人姓名
     *
     * @param updateByname 冗余字段：更新人姓名
     */
    public void setUpdateByname(String updateByname) {
        this.updateByname = updateByname == null ? null : updateByname.trim();
    }

    /**
     * 获取票号
     *
     * @return ticket_no - 票号
     */
    public String getTicketNo() {
        return ticketNo;
    }

    /**
     * 设置票号
     *
     * @param ticketNo 票号
     */
    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo == null ? null : ticketNo.trim();
    }

    /**
     * 获取票面金额
     *
     * @return ticket_amount - 票面金额
     */
    public BigDecimal getTicketAmount() {
        return ticketAmount;
    }

    /**
     * 设置票面金额
     *
     * @param ticketAmount 票面金额
     */
    public void setTicketAmount(BigDecimal ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    /**
     * 获取到期日期
     *
     * @return ticket_terminal_date - 到期日期
     */
    public Date getTicketTerminalDate() {
        return ticketTerminalDate;
    }

    /**
     * 设置到期日期
     *
     * @param ticketTerminalDate 到期日期
     */
    public void setTicketTerminalDate(Date ticketTerminalDate) {
        this.ticketTerminalDate = ticketTerminalDate;
    }

    /**
     * 获取出票日
     *
     * @return ticket_create_date - 出票日
     */
    public Date getTicketCreateDate() {
        return ticketCreateDate;
    }

    /**
     * 设置出票日
     *
     * @param ticketCreateDate 出票日
     */
    public void setTicketCreateDate(Date ticketCreateDate) {
        this.ticketCreateDate = ticketCreateDate;
    }

    /**
     * 获取出票人全称
     *
     * @return ticket_send_name - 出票人全称
     */
    public String getTicketSendName() {
        return ticketSendName;
    }

    /**
     * 设置出票人全称
     *
     * @param ticketSendName 出票人全称
     */
    public void setTicketSendName(String ticketSendName) {
        this.ticketSendName = ticketSendName == null ? null : ticketSendName.trim();
    }

    /**
     * 获取出票人账号
     *
     * @return ticket_send_no - 出票人账号
     */
    public String getTicketSendNo() {
        return ticketSendNo;
    }

    /**
     * 设置出票人账号
     *
     * @param ticketSendNo 出票人账号
     */
    public void setTicketSendNo(String ticketSendNo) {
        this.ticketSendNo = ticketSendNo == null ? null : ticketSendNo.trim();
    }

    /**
     * 获取出票人开户银行
     *
     * @return ticket_send_bank_name - 出票人开户银行
     */
    public String getTicketSendBankName() {
        return ticketSendBankName;
    }

    /**
     * 设置出票人开户银行
     *
     * @param ticketSendBankName 出票人开户银行
     */
    public void setTicketSendBankName(String ticketSendBankName) {
        this.ticketSendBankName = ticketSendBankName == null ? null : ticketSendBankName.trim();
    }

    /**
     * 获取收票人全称
     *
     * @return ticket_receive_name - 收票人全称
     */
    public String getTicketReceiveName() {
        return ticketReceiveName;
    }

    /**
     * 设置收票人全称
     *
     * @param ticketReceiveName 收票人全称
     */
    public void setTicketReceiveName(String ticketReceiveName) {
        this.ticketReceiveName = ticketReceiveName == null ? null : ticketReceiveName.trim();
    }

    /**
     * 获取收票人账号
     *
     * @return ticket_receive_no - 收票人账号
     */
    public String getTicketReceiveNo() {
        return ticketReceiveNo;
    }

    /**
     * 设置收票人账号
     *
     * @param ticketReceiveNo 收票人账号
     */
    public void setTicketReceiveNo(String ticketReceiveNo) {
        this.ticketReceiveNo = ticketReceiveNo == null ? null : ticketReceiveNo.trim();
    }

    /**
     * 获取收票人开户银行
     *
     * @return ticket_receive_bank_name - 收票人开户银行
     */
    public String getTicketReceiveBankName() {
        return ticketReceiveBankName;
    }

    /**
     * 设置收票人开户银行
     *
     * @param ticketReceiveBankName 收票人开户银行
     */
    public void setTicketReceiveBankName(String ticketReceiveBankName) {
        this.ticketReceiveBankName = ticketReceiveBankName == null ? null : ticketReceiveBankName.trim();
    }

    /**
     * 获取承兑人账号
     *
     * @return ticket_accept_no - 承兑人账号
     */
    public String getTicketAcceptNo() {
        return ticketAcceptNo;
    }

    /**
     * 设置承兑人账号
     *
     * @param ticketAcceptNo 承兑人账号
     */
    public void setTicketAcceptNo(String ticketAcceptNo) {
        this.ticketAcceptNo = ticketAcceptNo == null ? null : ticketAcceptNo.trim();
    }

    /**
     * 获取承兑人全称
     *
     * @return ticket_accept_name - 承兑人全称
     */
    public String getTicketAcceptName() {
        return ticketAcceptName;
    }

    /**
     * 设置承兑人全称
     *
     * @param ticketAcceptName 承兑人全称
     */
    public void setTicketAcceptName(String ticketAcceptName) {
        this.ticketAcceptName = ticketAcceptName == null ? null : ticketAcceptName.trim();
    }

    /**
     * 获取承兑人开户行行号
     *
     * @return ticket_accept_bank_no - 承兑人开户行行号
     */
    public String getTicketAcceptBankNo() {
        return ticketAcceptBankNo;
    }

    /**
     * 设置承兑人开户行行号
     *
     * @param ticketAcceptBankNo 承兑人开户行行号
     */
    public void setTicketAcceptBankNo(String ticketAcceptBankNo) {
        this.ticketAcceptBankNo = ticketAcceptBankNo == null ? null : ticketAcceptBankNo.trim();
    }

    /**
     * 获取承兑人开户行名称
     *
     * @return ticket_accept_bank_name - 承兑人开户行名称
     */
    public String getTicketAcceptBankName() {
        return ticketAcceptBankName;
    }

    /**
     * 设置承兑人开户行名称
     *
     * @param ticketAcceptBankName 承兑人开户行名称
     */
    public void setTicketAcceptBankName(String ticketAcceptBankName) {
        this.ticketAcceptBankName = ticketAcceptBankName == null ? null : ticketAcceptBankName.trim();
    }

    /**
     * 获取能否转让
     *
     * @return ticket_is_sell - 能否转让
     */
    public String getTicketIsSell() {
        return ticketIsSell;
    }

    /**
     * 设置能否转让
     *
     * @param ticketIsSell 能否转让
     */
    public void setTicketIsSell(String ticketIsSell) {
        this.ticketIsSell = ticketIsSell == null ? null : ticketIsSell.trim();
    }

    /**
     * 获取承兑协议编号/交易合同号
     *
     * @return ticket_deal_no - 承兑协议编号/交易合同号
     */
    public String getTicketDealNo() {
        return ticketDealNo;
    }

    /**
     * 设置承兑协议编号/交易合同号
     *
     * @param ticketDealNo 承兑协议编号/交易合同号
     */
    public void setTicketDealNo(String ticketDealNo) {
        this.ticketDealNo = ticketDealNo == null ? null : ticketDealNo.trim();
    }

    /**
     * 获取出票保证人姓名
     *
     * @return ticket_send_guarantee - 出票保证人姓名
     */
    public String getTicketSendGuarantee() {
        return ticketSendGuarantee;
    }

    /**
     * 设置出票保证人姓名
     *
     * @param ticketSendGuarantee 出票保证人姓名
     */
    public void setTicketSendGuarantee(String ticketSendGuarantee) {
        this.ticketSendGuarantee = ticketSendGuarantee == null ? null : ticketSendGuarantee.trim();
    }

    /**
     * 获取承兑保证人姓名
     *
     * @return ticket_accept_guarantee - 承兑保证人姓名
     */
    public String getTicketAcceptGuarantee() {
        return ticketAcceptGuarantee;
    }

    /**
     * 设置承兑保证人姓名
     *
     * @param ticketAcceptGuarantee 承兑保证人姓名
     */
    public void setTicketAcceptGuarantee(String ticketAcceptGuarantee) {
        this.ticketAcceptGuarantee = ticketAcceptGuarantee == null ? null : ticketAcceptGuarantee.trim();
    }

    /**
     * 获取出票人信用等级
     *
     * @return ticket_send_credit_level - 出票人信用等级
     */
    public String getTicketSendCreditLevel() {
        return ticketSendCreditLevel;
    }

    /**
     * 设置出票人信用等级
     *
     * @param ticketSendCreditLevel 出票人信用等级
     */
    public void setTicketSendCreditLevel(String ticketSendCreditLevel) {
        this.ticketSendCreditLevel = ticketSendCreditLevel == null ? null : ticketSendCreditLevel.trim();
    }

    /**
     * 获取出票人评级到期日
     *
     * @return ticket_send_credit_terminal_date - 出票人评级到期日
     */
    public String getTicketSendCreditTerminalDate() {
        return ticketSendCreditTerminalDate;
    }

    /**
     * 设置出票人评级到期日
     *
     * @param ticketSendCreditTerminalDate 出票人评级到期日
     */
    public void setTicketSendCreditTerminalDate(String ticketSendCreditTerminalDate) {
        this.ticketSendCreditTerminalDate = ticketSendCreditTerminalDate == null ? null : ticketSendCreditTerminalDate.trim();
    }

    /**
     * 获取承兑人信用等级
     *
     * @return ticket_accept_credit_level - 承兑人信用等级
     */
    public String getTicketAcceptCreditLevel() {
        return ticketAcceptCreditLevel;
    }

    /**
     * 设置承兑人信用等级
     *
     * @param ticketAcceptCreditLevel 承兑人信用等级
     */
    public void setTicketAcceptCreditLevel(String ticketAcceptCreditLevel) {
        this.ticketAcceptCreditLevel = ticketAcceptCreditLevel == null ? null : ticketAcceptCreditLevel.trim();
    }

    /**
     * 获取承兑人评级到期日
     *
     * @return ticket_accept_credit_terminal_date - 承兑人评级到期日
     */
    public String getTicketAcceptCreditTerminalDate() {
        return ticketAcceptCreditTerminalDate;
    }

    /**
     * 设置承兑人评级到期日
     *
     * @param ticketAcceptCreditTerminalDate 承兑人评级到期日
     */
    public void setTicketAcceptCreditTerminalDate(String ticketAcceptCreditTerminalDate) {
        this.ticketAcceptCreditTerminalDate = ticketAcceptCreditTerminalDate == null ? null : ticketAcceptCreditTerminalDate.trim();
    }

    /**
     * 获取票面正面图片
     *
     * @return ticket_front_img_url - 票面正面图片
     */
    public String getTicketFrontImgUrl() {
        return ticketFrontImgUrl;
    }

    /**
     * 设置票面正面图片
     *
     * @param ticketFrontImgUrl 票面正面图片
     */
    public void setTicketFrontImgUrl(String ticketFrontImgUrl) {
        this.ticketFrontImgUrl = ticketFrontImgUrl == null ? null : ticketFrontImgUrl.trim();
    }

    /**
     * 获取票面被面图片
     *
     * @return ticket_back_img_url - 票面被面图片
     */
    public String getTicketBackImgUrl() {
        return ticketBackImgUrl;
    }

    /**
     * 设置票面被面图片
     *
     * @param ticketBackImgUrl 票面被面图片
     */
    public void setTicketBackImgUrl(String ticketBackImgUrl) {
        this.ticketBackImgUrl = ticketBackImgUrl == null ? null : ticketBackImgUrl.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取票据类型：1电银2电商
     *
     * @return ticket_type - 票据类型：1电银2电商
     */
    public String getTicketType() {
        return ticketType;
    }

    /**
     * 设置票据类型：1电银2电商
     *
     * @param ticketType 票据类型：1电银2电商
     */
    public void setTicketType(String ticketType) {
        this.ticketType = ticketType == null ? null : ticketType.trim();
    }

    /**
     * 获取银行域名
     *
     * @return bank_domain - 银行域名
     */
    public String getBankDomain() {
        return bankDomain;
    }

    /**
     * 设置银行域名
     *
     * @param bankDomain 银行域名
     */
    public void setBankDomain(String bankDomain) {
        this.bankDomain = bankDomain == null ? null : bankDomain.trim();
    }

    /**
     * 获取票据瑕疵 1绝对记载事项缺失 2不可转让
     *
     * @return ticket_flaw - 票据瑕疵 1绝对记载事项缺失 2不可转让
     */
    public String getTicketFlaw() {
        return ticketFlaw;
    }

    /**
     * 设置票据瑕疵 1绝对记载事项缺失 2不可转让
     *
     * @param ticketFlaw 票据瑕疵 1绝对记载事项缺失 2不可转让
     */
    public void setTicketFlaw(String ticketFlaw) {
        this.ticketFlaw = ticketFlaw == null ? null : ticketFlaw.trim();
    }

    /**
     * 获取票据状态
     *
     * @return ticket_state - 票据状态
     */
    public String getTicketState() {
        return ticketState;
    }

    /**
     * 设置票据状态
     *
     * @param ticketState 票据状态
     */
    public void setTicketState(String ticketState) {
        this.ticketState = ticketState == null ? null : ticketState.trim();
    }

    /**
     * 获取检验结果：0正常1正面异常2背面异常3正背面异常
     *
     * @return check_result - 检验结果：0正常1正面异常2背面异常3正背面异常
     */
    public String getCheckResult() {
        return checkResult;
    }

    /**
     * 设置检验结果：0正常1正面异常2背面异常3正背面异常
     *
     * @param checkResult 检验结果：0正常1正面异常2背面异常3正背面异常
     */
    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult == null ? null : checkResult.trim();
    }

    /**
     * 获取背书次数
     *
     * @return recite_account - 背书次数
     */
    public Integer getReciteAccount() {
        return reciteAccount;
    }

    /**
     * 设置背书次数
     *
     * @param reciteAccount 背书次数
     */
    public void setReciteAccount(Integer reciteAccount) {
        this.reciteAccount = reciteAccount;
    }

    /**
     * 获取承兑方开户行简称
     *
     * @return ticket_accept_bank_shortname - 承兑方开户行简称
     */
    public String getTicketAcceptBankShortname() {
        return ticketAcceptBankShortname;
    }

    /**
     * 设置承兑方开户行简称
     *
     * @param ticketAcceptBankShortname 承兑方开户行简称
     */
    public void setTicketAcceptBankShortname(String ticketAcceptBankShortname) {
        this.ticketAcceptBankShortname = ticketAcceptBankShortname == null ? null : ticketAcceptBankShortname.trim();
    }

    /**
     * 获取承兑人类别
     *
     * @return ticket_accept_type - 承兑人类别
     */
    public String getTicketAcceptType() {
        return ticketAcceptType;
    }

    /**
     * 设置承兑人类别
     *
     * @param ticketAcceptType 承兑人类别
     */
    public void setTicketAcceptType(String ticketAcceptType) {
        this.ticketAcceptType = ticketAcceptType == null ? null : ticketAcceptType.trim();
    }

    /**
     * 获取处理状态1待入库2入库中3待签收4已签收5已拒签6已出库
     *
     * @return assign_state - 处理状态1待入库2入库中3待签收4已签收5已拒签6已出库
     */
    public String getAssignState() {
        return assignState;
    }

    /**
     * 设置处理状态1待入库2入库中3待签收4已签收5已拒签6已出库
     *
     * @param assignState 处理状态1待入库2入库中3待签收4已签收5已拒签6已出库
     */
    public void setAssignState(String assignState) {
        this.assignState = assignState == null ? null : assignState.trim();
    }

    /**
     * 获取调整天
     *
     * @return adjustment_days - 调整天
     */
    public Integer getAdjustmentDays() {
        return adjustmentDays;
    }

    /**
     * 设置调整天
     *
     * @param adjustmentDays 调整天
     */
    public void setAdjustmentDays(Integer adjustmentDays) {
        this.adjustmentDays = adjustmentDays;
    }

    /**
     * 获取出票人承诺
     *
     * @return ticket_send_promise - 出票人承诺
     */
    public String getTicketSendPromise() {
        return ticketSendPromise;
    }

    /**
     * 设置出票人承诺
     *
     * @param ticketSendPromise 出票人承诺
     */
    public void setTicketSendPromise(String ticketSendPromise) {
        this.ticketSendPromise = ticketSendPromise == null ? null : ticketSendPromise.trim();
    }

    /**
     * 获取保证人姓名
     *
     * @return promiser - 保证人姓名
     */
    public String getPromiser() {
        return promiser;
    }

    /**
     * 设置保证人姓名
     *
     * @param promiser 保证人姓名
     */
    public void setPromiser(String promiser) {
        this.promiser = promiser == null ? null : promiser.trim();
    }

    /**
     * 获取保证人地址
     *
     * @return promiser_address - 保证人地址
     */
    public String getPromiserAddress() {
        return promiserAddress;
    }

    /**
     * 设置保证人地址
     *
     * @param promiserAddress 保证人地址
     */
    public void setPromiserAddress(String promiserAddress) {
        this.promiserAddress = promiserAddress == null ? null : promiserAddress.trim();
    }

    /**
     * 获取保证日期
     *
     * @return promiser_date - 保证日期
     */
    public String getPromiserDate() {
        return promiserDate;
    }

    /**
     * 设置保证日期
     *
     * @param promiserDate 保证日期
     */
    public void setPromiserDate(String promiserDate) {
        this.promiserDate = promiserDate == null ? null : promiserDate.trim();
    }

    /**
     * 获取承兑日期
     *
     * @return ticket_accept_date - 承兑日期
     */
    public String getTicketAcceptDate() {
        return ticketAcceptDate;
    }

    /**
     * 设置承兑日期
     *
     * @param ticketAcceptDate 承兑日期
     */
    public void setTicketAcceptDate(String ticketAcceptDate) {
        this.ticketAcceptDate = ticketAcceptDate == null ? null : ticketAcceptDate.trim();
    }

    /**
     * 获取承兑人承诺
     *
     * @return ticket_accept_promise - 承兑人承诺
     */
    public String getTicketAcceptPromise() {
        return ticketAcceptPromise;
    }

    /**
     * 设置承兑人承诺
     *
     * @param ticketAcceptPromise 承兑人承诺
     */
    public void setTicketAcceptPromise(String ticketAcceptPromise) {
        this.ticketAcceptPromise = ticketAcceptPromise == null ? null : ticketAcceptPromise.trim();
    }

    /**
     * 获取票面金额中文
     *
     * @return ticket_amount_chinese - 票面金额中文
     */
    public String getTicketAmountChinese() {
        return ticketAmountChinese;
    }

    /**
     * 设置票面金额中文
     *
     * @param ticketAmountChinese 票面金额中文
     */
    public void setTicketAmountChinese(String ticketAmountChinese) {
        this.ticketAmountChinese = ticketAmountChinese == null ? null : ticketAmountChinese.trim();
    }

    /**
     * 获取承兑汇票中文名字
     *
     * @return ticket_type_name - 承兑汇票中文名字
     */
    public String getTicketTypeName() {
        return ticketTypeName;
    }

    /**
     * 设置承兑汇票中文名字
     *
     * @param ticketTypeName 承兑汇票中文名字
     */
    public void setTicketTypeName(String ticketTypeName) {
        this.ticketTypeName = ticketTypeName == null ? null : ticketTypeName.trim();
    }

    /**
     * 获取提交人id
     *
     * @return submit_user_id - 提交人id
     */
    public Long getSubmitUserId() {
        return submitUserId;
    }

    /**
     * 设置提交人id
     *
     * @param submitUserId 提交人id
     */
    public void setSubmitUserId(Long submitUserId) {
        this.submitUserId = submitUserId;
    }

    /**
     * 获取提交人姓名
     *
     * @return submit_user_name - 提交人姓名
     */
    public String getSubmitUserName() {
        return submitUserName;
    }

    /**
     * 设置提交人姓名
     *
     * @param submitUserName 提交人姓名
     */
    public void setSubmitUserName(String submitUserName) {
        this.submitUserName = submitUserName == null ? null : submitUserName.trim();
    }

    /**
     * 获取购入员工ID
     *
     * @return buy_user_id - 购入员工ID
     */
    public Long getBuyUserId() {
        return buyUserId;
    }

    /**
     * 设置购入员工ID
     *
     * @param buyUserId 购入员工ID
     */
    public void setBuyUserId(Long buyUserId) {
        this.buyUserId = buyUserId;
    }

    /**
     * 获取购入员工姓名
     *
     * @return buy_user_name - 购入员工姓名
     */
    public String getBuyUserName() {
        return buyUserName;
    }

    /**
     * 设置购入员工姓名
     *
     * @param buyUserName 购入员工姓名
     */
    public void setBuyUserName(String buyUserName) {
        this.buyUserName = buyUserName == null ? null : buyUserName.trim();
    }

    /**
     * 获取购入价类型 1直扣 2每十万扣款 3年息 4无
     *
     * @return buy_type - 购入价类型 1直扣 2每十万扣款 3年息 4无
     */
    public String getBuyType() {
        return buyType;
    }

    /**
     * 设置购入价类型 1直扣 2每十万扣款 3年息 4无
     *
     * @param buyType 购入价类型 1直扣 2每十万扣款 3年息 4无
     */
    public void setBuyType(String buyType) {
        this.buyType = buyType == null ? null : buyType.trim();
    }

    /**
     * 获取拟售价类型 1直扣 2每十万扣款 3年息 4无
     *
     * @return expect_type - 拟售价类型 1直扣 2每十万扣款 3年息 4无
     */
    public String getExpectType() {
        return expectType;
    }

    /**
     * 设置拟售价类型 1直扣 2每十万扣款 3年息 4无
     *
     * @param expectType 拟售价类型 1直扣 2每十万扣款 3年息 4无
     */
    public void setExpectType(String expectType) {
        this.expectType = expectType == null ? null : expectType.trim();
    }

    /**
     * 获取拟购价（年息）
     *
     * @return buy_point - 拟购价（年息）
     */
    public BigDecimal getBuyPoint() {
        return buyPoint;
    }

    /**
     * 设置拟购价（年息）
     *
     * @param buyPoint 拟购价（年息）
     */
    public void setBuyPoint(BigDecimal buyPoint) {
        this.buyPoint = buyPoint;
    }

    /**
     * 获取拟售价（年息）
     *
     * @return expect_point - 拟售价（年息）
     */
    public BigDecimal getExpectPoint() {
        return expectPoint;
    }

    /**
     * 设置拟售价（年息）
     *
     * @param expectPoint 拟售价（年息）
     */
    public void setExpectPoint(BigDecimal expectPoint) {
        this.expectPoint = expectPoint;
    }

    /**
     * 获取购入价直扣
     *
     * @return buy_direct_deduction - 购入价直扣
     */
    public BigDecimal getBuyDirectDeduction() {
        return buyDirectDeduction;
    }

    /**
     * 设置购入价直扣
     *
     * @param buyDirectDeduction 购入价直扣
     */
    public void setBuyDirectDeduction(BigDecimal buyDirectDeduction) {
        this.buyDirectDeduction = buyDirectDeduction;
    }

    /**
     * 获取购入价每10万扣款
     *
     * @return buy_lakh_debit - 购入价每10万扣款
     */
    public BigDecimal getBuyLakhDebit() {
        return buyLakhDebit;
    }

    /**
     * 设置购入价每10万扣款
     *
     * @param buyLakhDebit 购入价每10万扣款
     */
    public void setBuyLakhDebit(BigDecimal buyLakhDebit) {
        this.buyLakhDebit = buyLakhDebit;
    }

    /**
     * 获取购入价手续费
     *
     * @return buy_handle_charge - 购入价手续费
     */
    public BigDecimal getBuyHandleCharge() {
        return buyHandleCharge;
    }

    /**
     * 设置购入价手续费
     *
     * @param buyHandleCharge 购入价手续费
     */
    public void setBuyHandleCharge(BigDecimal buyHandleCharge) {
        this.buyHandleCharge = buyHandleCharge;
    }

    /**
     * 获取拟售价直扣
     *
     * @return expect_direct_deduction - 拟售价直扣
     */
    public BigDecimal getExpectDirectDeduction() {
        return expectDirectDeduction;
    }

    /**
     * 设置拟售价直扣
     *
     * @param expectDirectDeduction 拟售价直扣
     */
    public void setExpectDirectDeduction(BigDecimal expectDirectDeduction) {
        this.expectDirectDeduction = expectDirectDeduction;
    }

    /**
     * 获取拟售价每十万扣款
     *
     * @return expect_lakh_debit - 拟售价每十万扣款
     */
    public BigDecimal getExpectLakhDebit() {
        return expectLakhDebit;
    }

    /**
     * 设置拟售价每十万扣款
     *
     * @param expectLakhDebit 拟售价每十万扣款
     */
    public void setExpectLakhDebit(BigDecimal expectLakhDebit) {
        this.expectLakhDebit = expectLakhDebit;
    }

    /**
     * 获取拟售价手续费
     *
     * @return expect_handle_charge - 拟售价手续费
     */
    public BigDecimal getExpectHandleCharge() {
        return expectHandleCharge;
    }

    /**
     * 设置拟售价手续费
     *
     * @param expectHandleCharge 拟售价手续费
     */
    public void setExpectHandleCharge(BigDecimal expectHandleCharge) {
        this.expectHandleCharge = expectHandleCharge;
    }

    /**
     * 获取购入数量
     *
     * @return stock_num - 购入数量
     */
    public Integer getStockNum() {
        return stockNum;
    }

    /**
     * 设置购入数量
     *
     * @param stockNum 购入数量
     */
    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    /**
     * 获取提交入库备注
     *
     * @return commit_common - 提交入库备注
     */
    public String getCommitCommon() {
        return commitCommon;
    }

    /**
     * 设置提交入库备注
     *
     * @param commitCommon 提交入库备注
     */
    public void setCommitCommon(String commitCommon) {
        this.commitCommon = commitCommon == null ? null : commitCommon.trim();
    }

    /**
     * 获取警示
     *
     * @return ticket_warnings - 警示
     */
    public String getTicketWarnings() {
        return ticketWarnings;
    }

    /**
     * 设置警示
     *
     * @param ticketWarnings 警示
     */
    public void setTicketWarnings(String ticketWarnings) {
        this.ticketWarnings = ticketWarnings == null ? null : ticketWarnings.trim();
    }

    /**
     * 获取最后一手背书人
     *
     * @return last_recite_name - 最后一手背书人
     */
    public String getLastReciteName() {
        return lastReciteName;
    }

    /**
     * 设置最后一手背书人
     *
     * @param lastReciteName 最后一手背书人
     */
    public void setLastReciteName(String lastReciteName) {
        this.lastReciteName = lastReciteName == null ? null : lastReciteName.trim();
    }

    /**
     * 获取当前所在交易户，签收后的最后一手被背书人
     *
     * @return assgin_endorsee_name - 当前所在交易户，签收后的最后一手被背书人
     */
    public String getAssginEndorseeName() {
        return assginEndorseeName;
    }

    /**
     * 设置当前所在交易户，签收后的最后一手被背书人
     *
     * @param assginEndorseeName 当前所在交易户，签收后的最后一手被背书人
     */
    public void setAssginEndorseeName(String assginEndorseeName) {
        this.assginEndorseeName = assginEndorseeName == null ? null : assginEndorseeName.trim();
    }

    /**
     * 获取总金额
     *
     * @return total_amount - 总金额
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * 设置总金额
     *
     * @param totalAmount 总金额
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return repeat_count
     */
    public Integer getRepeatCount() {
        return repeatCount;
    }

    /**
     * @param repeatCount
     */
    public void setRepeatCount(Integer repeatCount) {
        this.repeatCount = repeatCount;
    }

    /**
     * @return big_return_count
     */
    public Integer getBigReturnCount() {
        return bigReturnCount;
    }

    /**
     * @param bigReturnCount
     */
    public void setBigReturnCount(Integer bigReturnCount) {
        this.bigReturnCount = bigReturnCount;
    }

    /**
     * @return small_return_count
     */
    public Integer getSmallReturnCount() {
        return smallReturnCount;
    }

    /**
     * @param smallReturnCount
     */
    public void setSmallReturnCount(Integer smallReturnCount) {
        this.smallReturnCount = smallReturnCount;
    }

    /**
     * @return pledge_count
     */
    public Integer getPledgeCount() {
        return pledgeCount;
    }

    /**
     * @param pledgeCount
     */
    public void setPledgeCount(Integer pledgeCount) {
        this.pledgeCount = pledgeCount;
    }

    /**
     * @return total_page
     */
    public Integer getTotalPage() {
        return totalPage;
    }

    /**
     * @param totalPage
     */
    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    /**
     * 获取背书人上出现出票人
     *
     * @return has_ticket_send2 - 背书人上出现出票人
     */
    public Integer getHasTicketSend2() {
        return hasTicketSend2;
    }

    /**
     * 设置背书人上出现出票人
     *
     * @param hasTicketSend2 背书人上出现出票人
     */
    public void setHasTicketSend2(Integer hasTicketSend2) {
        this.hasTicketSend2 = hasTicketSend2;
    }

    /**
     * 获取背书人上出现收票人
     *
     * @return has_ticket_receive2 - 背书人上出现收票人
     */
    public Integer getHasTicketReceive2() {
        return hasTicketReceive2;
    }

    /**
     * 设置背书人上出现收票人
     *
     * @param hasTicketReceive2 背书人上出现收票人
     */
    public void setHasTicketReceive2(Integer hasTicketReceive2) {
        this.hasTicketReceive2 = hasTicketReceive2;
    }

    /**
     * 获取签收二次识别开关状态 1:打开 0：关闭
     *
     * @return second_confirm_flag - 签收二次识别开关状态 1:打开 0：关闭
     */
    public String getSecondConfirmFlag() {
        return secondConfirmFlag;
    }

    /**
     * 设置签收二次识别开关状态 1:打开 0：关闭
     *
     * @param secondConfirmFlag 签收二次识别开关状态 1:打开 0：关闭
     */
    public void setSecondConfirmFlag(String secondConfirmFlag) {
        this.secondConfirmFlag = secondConfirmFlag == null ? null : secondConfirmFlag.trim();
    }

    /**
     * 获取内部记事
     *
     * @return internal_commons - 内部记事
     */
    public String getInternalCommons() {
        return internalCommons;
    }

    /**
     * 设置内部记事
     *
     * @param internalCommons 内部记事
     */
    public void setInternalCommons(String internalCommons) {
        this.internalCommons = internalCommons == null ? null : internalCommons.trim();
    }

    /**
     * 获取二次签收正面临时生成的图片
     *
     * @return second_sign_front_img_url - 二次签收正面临时生成的图片
     */
    public String getSecondSignFrontImgUrl() {
        return secondSignFrontImgUrl;
    }

    /**
     * 设置二次签收正面临时生成的图片
     *
     * @param secondSignFrontImgUrl 二次签收正面临时生成的图片
     */
    public void setSecondSignFrontImgUrl(String secondSignFrontImgUrl) {
        this.secondSignFrontImgUrl = secondSignFrontImgUrl == null ? null : secondSignFrontImgUrl.trim();
    }

    /**
     * 获取打码的正面图片
     *
     * @return ticket_front_mask_img_url - 打码的正面图片
     */
    public String getTicketFrontMaskImgUrl() {
        return ticketFrontMaskImgUrl;
    }

    /**
     * 设置打码的正面图片
     *
     * @param ticketFrontMaskImgUrl 打码的正面图片
     */
    public void setTicketFrontMaskImgUrl(String ticketFrontMaskImgUrl) {
        this.ticketFrontMaskImgUrl = ticketFrontMaskImgUrl == null ? null : ticketFrontMaskImgUrl.trim();
    }

    /**
     * 获取期限分类0-不足月 1-半年 2-一年 3-超期
     *
     * @return deadline_classify - 期限分类0-不足月 1-半年 2-一年 3-超期
     */
    public String getDeadlineClassify() {
        return deadlineClassify;
    }

    /**
     * 设置期限分类0-不足月 1-半年 2-一年 3-超期
     *
     * @param deadlineClassify 期限分类0-不足月 1-半年 2-一年 3-超期
     */
    public void setDeadlineClassify(String deadlineClassify) {
        this.deadlineClassify = deadlineClassify == null ? null : deadlineClassify.trim();
    }

    /**
     * 获取"0" :不占资  "1":占资
     *
     * @return capital_type - "0" :不占资  "1":占资
     */
    public String getCapitalType() {
        return capitalType;
    }

    /**
     * 设置"0" :不占资  "1":占资
     *
     * @param capitalType "0" :不占资  "1":占资
     */
    public void setCapitalType(String capitalType) {
        this.capitalType = capitalType == null ? null : capitalType.trim();
    }

    /**
     * @return ticket_client
     */
    public String getTicketClient() {
        return ticketClient;
    }

    /**
     * @param ticketClient
     */
    public void setTicketClient(String ticketClient) {
        this.ticketClient = ticketClient == null ? null : ticketClient.trim();
    }

    /**
     * 获取入库时间
     *
     * @return stock_in_time - 入库时间
     */
    public Date getStockInTime() {
        return stockInTime;
    }

    /**
     * 设置入库时间
     *
     * @param stockInTime 入库时间
     */
    public void setStockInTime(Date stockInTime) {
        this.stockInTime = stockInTime;
    }

    /**
     * 获取出库时间
     *
     * @return stock_out_time - 出库时间
     */
    public Date getStockOutTime() {
        return stockOutTime;
    }

    /**
     * 设置出库时间
     *
     * @param stockOutTime 出库时间
     */
    public void setStockOutTime(Date stockOutTime) {
        this.stockOutTime = stockOutTime;
    }

    /**
     * 获取识别时是否开启显示警示"0" :未开启  "1":开启
     *
     * @return show_warning_flag - 识别时是否开启显示警示"0" :未开启  "1":开启
     */
    public String getShowWarningFlag() {
        return showWarningFlag;
    }

    /**
     * 设置识别时是否开启显示警示"0" :未开启  "1":开启
     *
     * @param showWarningFlag 识别时是否开启显示警示"0" :未开启  "1":开启
     */
    public void setShowWarningFlag(String showWarningFlag) {
        this.showWarningFlag = showWarningFlag == null ? null : showWarningFlag.trim();
    }

    /**
     * @return user_has_read
     */
    public Boolean getUserHasRead() {
        return userHasRead;
    }

    /**
     * @param userHasRead
     */
    public void setUserHasRead(Boolean userHasRead) {
        this.userHasRead = userHasRead;
    }

    /**
     * @return batch_md5
     */
    public String getBatchMd5() {
        return batchMd5;
    }

    /**
     * @param batchMd5
     */
    public void setBatchMd5(String batchMd5) {
        this.batchMd5 = batchMd5 == null ? null : batchMd5.trim();
    }

    /**
     * @return batch_parent_id
     */
    public Long getBatchParentId() {
        return batchParentId;
    }

    /**
     * @param batchParentId
     */
    public void setBatchParentId(Long batchParentId) {
        this.batchParentId = batchParentId;
    }

    /**
     * @return batch_count
     */
    public Integer getBatchCount() {
        return batchCount;
    }

    /**
     * @param batchCount
     */
    public void setBatchCount(Integer batchCount) {
        this.batchCount = batchCount;
    }

    /**
     * @return catch_mode
     */
    public Integer getCatchMode() {
        return catchMode;
    }

    /**
     * @param catchMode
     */
    public void setCatchMode(Integer catchMode) {
        this.catchMode = catchMode;
    }

    /**
     * 获取识别时是否开启显示瑕疵"0" :未开启  "1":开启
     *
     * @return show_flaw_flag - 识别时是否开启显示瑕疵"0" :未开启  "1":开启
     */
    public String getShowFlawFlag() {
        return showFlawFlag;
    }

    /**
     * 设置识别时是否开启显示瑕疵"0" :未开启  "1":开启
     *
     * @param showFlawFlag 识别时是否开启显示瑕疵"0" :未开启  "1":开启
     */
    public void setShowFlawFlag(String showFlawFlag) {
        this.showFlawFlag = showFlawFlag == null ? null : showFlawFlag.trim();
    }

    /**
     * 获取调度人名字
     *
     * @return dispatch_byname - 调度人名字
     */
    public String getDispatchByname() {
        return dispatchByname;
    }

    /**
     * 设置调度人名字
     *
     * @param dispatchByname 调度人名字
     */
    public void setDispatchByname(String dispatchByname) {
        this.dispatchByname = dispatchByname == null ? null : dispatchByname.trim();
    }

    /**
     * 获取调度人id
     *
     * @return dispatch_by - 调度人id
     */
    public Long getDispatchBy() {
        return dispatchBy;
    }

    /**
     * 设置调度人id
     *
     * @param dispatchBy 调度人id
     */
    public void setDispatchBy(Long dispatchBy) {
        this.dispatchBy = dispatchBy;
    }

    /**
     * 获取拟售价年息天数
     *
     * @return expect_point_day - 拟售价年息天数
     */
    public Integer getExpectPointDay() {
        return expectPointDay;
    }

    /**
     * 设置拟售价年息天数
     *
     * @param expectPointDay 拟售价年息天数
     */
    public void setExpectPointDay(Integer expectPointDay) {
        this.expectPointDay = expectPointDay;
    }

    /**
     * 获取拟购价年息天数
     *
     * @return buy_point_day - 拟购价年息天数
     */
    public Integer getBuyPointDay() {
        return buyPointDay;
    }

    /**
     * 设置拟购价年息天数
     *
     * @param buyPointDay 拟购价年息天数
     */
    public void setBuyPointDay(Integer buyPointDay) {
        this.buyPointDay = buyPointDay;
    }

    /**
     * 获取挂票人员工姓名
     *
     * @return sell_user_name - 挂票人员工姓名
     */
    public String getSellUserName() {
        return sellUserName;
    }

    /**
     * 设置挂票人员工姓名
     *
     * @param sellUserName 挂票人员工姓名
     */
    public void setSellUserName(String sellUserName) {
        this.sellUserName = sellUserName == null ? null : sellUserName.trim();
    }

    /**
     * 获取挂票人员工id
     *
     * @return sell_user_id - 挂票人员工id
     */
    public Long getSellUserId() {
        return sellUserId;
    }

    /**
     * 设置挂票人员工id
     *
     * @param sellUserId 挂票人员工id
     */
    public void setSellUserId(Long sellUserId) {
        this.sellUserId = sellUserId;
    }

    /**
     * 获取删除时间
     *
     * @return delete_time - 删除时间
     */
    public Date getDeleteTime() {
        return deleteTime;
    }

    /**
     * 设置删除时间
     *
     * @param deleteTime 删除时间
     */
    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    /**
     * 获取供应链商票状态0,未认证1,认证中2,已认证4,已出库5,已到期
     *
     * @return chain_status - 供应链商票状态0,未认证1,认证中2,已认证4,已出库5,已到期
     */
    public String getChainStatus() {
        return chainStatus;
    }

    /**
     * 设置供应链商票状态0,未认证1,认证中2,已认证4,已出库5,已到期
     *
     * @param chainStatus 供应链商票状态0,未认证1,认证中2,已认证4,已出库5,已到期
     */
    public void setChainStatus(String chainStatus) {
        this.chainStatus = chainStatus == null ? null : chainStatus.trim();
    }

    /**
     * 获取数据类型 0-普通 1-链属票据
     *
     * @return data_type - 数据类型 0-普通 1-链属票据
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * 设置数据类型 0-普通 1-链属票据
     *
     * @param dataType 数据类型 0-普通 1-链属票据
     */
    public void setDataType(String dataType) {
        this.dataType = dataType == null ? null : dataType.trim();
    }

    /**
     * 获取贸易背景审核状态 1：审核通过 0：待审核  8:未提交 9：审核失败
     *
     * @return approval_status - 贸易背景审核状态 1：审核通过 0：待审核  8:未提交 9：审核失败
     */
    public String getApprovalStatus() {
        return approvalStatus;
    }

    /**
     * 设置贸易背景审核状态 1：审核通过 0：待审核  8:未提交 9：审核失败
     *
     * @param approvalStatus 贸易背景审核状态 1：审核通过 0：待审核  8:未提交 9：审核失败
     */
    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus == null ? null : approvalStatus.trim();
    }

    /**
     * 获取票据操作类型 1承兑2签收
     *
     * @return ticket_operate_type - 票据操作类型 1承兑2签收
     */
    public String getTicketOperateType() {
        return ticketOperateType;
    }

    /**
     * 设置票据操作类型 1承兑2签收
     *
     * @param ticketOperateType 票据操作类型 1承兑2签收
     */
    public void setTicketOperateType(String ticketOperateType) {
        this.ticketOperateType = ticketOperateType == null ? null : ticketOperateType.trim();
    }

    /**
     * 获取出票人是否已认证0未认证1已认证
     *
     * @return drawer_certify_status - 出票人是否已认证0未认证1已认证
     */
    public String getDrawerCertifyStatus() {
        return drawerCertifyStatus;
    }

    /**
     * 设置出票人是否已认证0未认证1已认证
     *
     * @param drawerCertifyStatus 出票人是否已认证0未认证1已认证
     */
    public void setDrawerCertifyStatus(String drawerCertifyStatus) {
        this.drawerCertifyStatus = drawerCertifyStatus == null ? null : drawerCertifyStatus.trim();
    }

    /**
     * 获取数据操作时间，比如认证，审核，等
     *
     * @return operate_time - 数据操作时间，比如认证，审核，等
     */
    public Date getOperateTime() {
        return operateTime;
    }

    /**
     * 设置数据操作时间，比如认证，审核，等
     *
     * @param operateTime 数据操作时间，比如认证，审核，等
     */
    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }

    /**
     * 获取上一次抓取时票据状态
     *
     * @return last_ticket_state - 上一次抓取时票据状态
     */
    public String getLastTicketState() {
        return lastTicketState;
    }

    /**
     * 设置上一次抓取时票据状态
     *
     * @param lastTicketState 上一次抓取时票据状态
     */
    public void setLastTicketState(String lastTicketState) {
        this.lastTicketState = lastTicketState == null ? null : lastTicketState.trim();
    }

    /**
     * @return batch_bank_domains
     */
    public String getBatchBankDomains() {
        return batchBankDomains;
    }

    /**
     * @param batchBankDomains
     */
    public void setBatchBankDomains(String batchBankDomains) {
        this.batchBankDomains = batchBankDomains == null ? null : batchBankDomains.trim();
    }

    /**
     * @return relative_batch_ids
     */
    public String getRelativeBatchIds() {
        return relativeBatchIds;
    }

    /**
     * @param relativeBatchIds
     */
    public void setRelativeBatchIds(String relativeBatchIds) {
        this.relativeBatchIds = relativeBatchIds == null ? null : relativeBatchIds.trim();
    }
}
