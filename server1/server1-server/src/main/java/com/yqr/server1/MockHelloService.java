package com.yqr.server1;


import com.yqr.server2.IHelloService;

public class MockHelloService implements IHelloService {
    @Override
    public String sayHello(String name) {
        return name+"sorry, 服务无法访问，返回降级数据";
    }
}
