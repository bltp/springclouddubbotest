package com.yqr.server1;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.yqr.server1.service.TicketAssignDelService;
import com.yqr.server2.IHelloService;
import org.apache.dubbo.common.extension.ExtensionLoader;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloControoler {
    @Reference(cluster = "failfast" ,mock = "com.yqr.server1.MockHelloService", check=false)
    private IHelloService iHelloService;
    @Value("${com.yqr.name}")
    private String name;

    @Autowired
    private TestConfig testConfig;
    
    @Autowired
    private TicketAssignDelService ticketAssignDelService;

    @GetMapping("/say")
    public String sayHello(){
        ExtensionLoader<Driver> extensionLoader=ExtensionLoader.getExtensionLoader(Driver.class);
        Driver driver=extensionLoader.getExtension("mysqlDriver");
        ticketAssignDelService.test();
        return iHelloService.sayHello(name);
    }
    
    
}


