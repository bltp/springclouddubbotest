package com.yqr.server1.dubbo;

import com.yqr.server1.IServer1;
import com.alibaba.dubbo.config.annotation.Service;
/**
 * @program: springclouddubbotest
 *
 * @description: 测试dubbo
 *
 * @author: Mr.Qian
 *
 * @create: 2021-03-16 13:48
 **/
@Service
public class IServer1Impl implements IServer1 {
    
    @Override
    public String test() {
        return "来自server1";
    }
}
