package com.yqr.server1;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component //添加component注解，spring扫描到这个类后会自动为其创建实例并注入配置
@ConfigurationProperties(prefix = "com.yqr") //prefix一般设置成模块的名
public class TestConfig {
    private String name;
}
