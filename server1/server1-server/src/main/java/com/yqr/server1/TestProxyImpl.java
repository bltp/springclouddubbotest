package com.yqr.server1;

/**
 * @program: springclouddubbotest
 * @description: 测试动态代理
 * @author: Mr.Qian
 * @create: 2021-03-31 14:32
 **/
public class TestProxyImpl implements TestProxy {
    
    @Override
    public int rInt() {
        return 0;
    }
}
