package com.yqr.server1.persistence.mapper;

import com.yqr.mapper.MyMapper;
import com.yqr.server1.persistence.model.TicketAssignDel;

public interface TicketAssignDelMapper extends MyMapper<TicketAssignDel> {
}
