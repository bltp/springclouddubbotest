package com.yqr.server1.service;

import com.yqr.common.exception.ServiceException;
import com.yqr.server1.persistence.mapper.TicketAssignDelMapper;
import com.yqr.server1.persistence.model.TicketAssignDel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
@Slf4j
public class TicketAssignDelService {

    @Autowired
    private TicketAssignDelMapper ticketAssignDelMapper;
    
    @Transactional(rollbackFor = Exception.class)
    public void test(){
        List<TicketAssignDel> list=ticketAssignDelMapper.selectAll();
        log.info("list={}",list);
        Example example=new Example(TicketAssignDel.class);
        example.createCriteria().andEqualTo("id","843213361507341864");
        TicketAssignDel ticketAssignDel=new TicketAssignDel();
        ticketAssignDel.setId(Long.valueOf("843213361507341864"));
        ticketAssignDel.setBuyUserName("qianqian");
        ticketAssignDelMapper.updateByPrimaryKeySelective(ticketAssignDel);
    }
}
