package com.yqr.server1;

import org.apache.dubbo.common.extension.SPI;

@SPI
public interface Driver {
    String connect();
}
