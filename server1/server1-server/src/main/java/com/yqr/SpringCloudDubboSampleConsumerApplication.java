package com.yqr;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

@Slf4j
@DubboComponentScan
@SpringBootApplication
public class SpringCloudDubboSampleConsumerApplication {

    public static void main(String[] args){
        ConfigurableApplicationContext context=SpringApplication.run(SpringCloudDubboSampleConsumerApplication.class,args);
        String info = context.getEnvironment().getProperty("com.yqr.name");
        log.info("info={}",info);
    }
}
