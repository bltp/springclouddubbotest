package com.yqr.oss;

import com.yqr.common.properties.OssProp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

/**
 * 用于配置OSSSTSKit
 *
 * @author tim
 */
@Configuration
@ConditionalOnProperty(prefix = OssProp.PREFIX, name = "enabled", havingValue = "true", matchIfMissing = false )
@EnableConfigurationProperties(OssProp.class)
@Slf4j
public class ConfigOsstsKit {
    @Autowired
    OssProp ossProp;

//    private static OssProp config;

    @PostConstruct
    public void init(){
        log.info("ossProp======================={}",ossProp);
        OSSSTSKit.BUCKET = ossProp.getAliyunOssBucket();
        OSSSTSKit.ENDPOINT = ossProp.getEndpoint();
        OSSSTSKit.REGION_CN = ossProp.getRegionCn();
        OSSSTSKit.STS_ACCESS_KEY_ID = ossProp.getStsAccessKeyId();
        OSSSTSKit.STS_ACCESS_KEY_SECRET = ossProp.getStsAccessKeySecret();
        OSSSTSKit.INTERNAL_ENDPOINT = ossProp.getInternalEndpoint();
        OSSSTSKit.STS_ROLE_ARN=ossProp.getStsRoleArn();
        OSSSTSKit.STS_DURATION_SECONDS=ossProp.getStsDurationSeconds();
        log.info("OSSSTSKit.BUCKET==========={}",OSSSTSKit.BUCKET);
    }
}
