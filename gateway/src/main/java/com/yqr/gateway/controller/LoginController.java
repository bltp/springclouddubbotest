package com.yqr.gateway.controller;

import com.yqr.common.result.R;
import com.yqr.gateway.persistence.model.Usr;
import com.yqr.gateway.service.MyUserDetailsServiceImpl;
import io.swagger.annotations.ApiParam;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: springclouddubbotest
 * @description: 登录相关接口
 * @author: Mr.Qian
 * @create: 2021-03-17 16:21
 **/
@RestController
public class LoginController {
    
    private MyUserDetailsServiceImpl myUserDetailsService;
    
    @RequestMapping("/hello")
    public String hello() {
        //这边我们,默认是返到templates下的login.html
        return "login";
    }
    
    @RequestMapping("/authentication/form")
    public R Login(
            @ApiParam(required = true, name = "mobile", value = "用户手机号") @RequestParam(value = "mobile", defaultValue = "", required = true) String mobile,
            @ApiParam(required = true, name = "password", value = "用户手机号") @RequestParam(value = "password", defaultValue = "", required = true) String password
    ){
        
        Usr usr=null;
        try{
            usr=(Usr)myUserDetailsService.loadUserByUsername(mobile);
        }catch (UsernameNotFoundException e){
            return R.fail("用户不存在");
        }
        return R.success();
    }
}
