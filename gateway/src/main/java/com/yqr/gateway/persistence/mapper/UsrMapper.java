package com.yqr.gateway.persistence.mapper;

import com.yqr.gateway.persistence.model.Usr;
import com.yqr.mapper.MyMapper;

public interface UsrMapper extends MyMapper<Usr> {
}