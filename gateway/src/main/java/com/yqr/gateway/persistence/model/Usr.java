package com.yqr.gateway.persistence.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@ToString
@ApiModel("")
public class Usr implements UserDetails {
    /**
     * 用户ID
     */
    @Id
    @Column(name = "usr_id")
    @ApiModelProperty("用户ID")
    private Long usrId;

    /**
     * 用户名
     */
    @Column(name = "usr_account")
    @ApiModelProperty("用户名")
    private String usrAccount;

    /**
     * 手机号码
     */
    @Column(name = "usr_mobile")
    @ApiModelProperty("手机号码")
    private String usrMobile;

    /**
     * 帐户密码
     */
    @Column(name = "usr_password")
    @ApiModelProperty("帐户密码")
    private String usrPassword;

    /**
     * 用户昵称
     */
    @Column(name = "usr_nick_name")
    @ApiModelProperty("用户昵称")
    private String usrNickName;

    /**
     * 头像
     */
    @Column(name = "head_pic")
    @ApiModelProperty("头像")
    private String headPic;

    /**
     * 用户等级 0-普通用户 1-认证企业用户
     */
    @Column(name = "group_id")
    @ApiModelProperty("用户等级 0-普通用户 1-认证企业用户")
    private Integer groupId;

    /**
     * 认证企业ID
     */
    @Column(name = "company_id")
    @ApiModelProperty("认证企业ID")
    private Long companyId;

    /**
     * 账号状态(-2、被删除，-1，被冻结，1：正常，2：注销)
     */
    @ApiModelProperty("账号状态(-2、被删除，-1，被冻结，1：正常，2：注销)")
    private Integer status;

    /**
     * 消息账号(0:未创建，1已创建)
     */
    @Column(name = "m_status")
    @ApiModelProperty("消息账号(0:未创建，1已创建)")
    private Integer mStatus;

    /**
     * 注册时间
     */
    @Column(name = "register_time")
    @ApiModelProperty("注册时间")
    private Date registerTime;

    /**
     * 注册IP
     */
    @Column(name = "register_ip")
    @ApiModelProperty("注册IP")
    private String registerIp;

    /**
     * 登陆IP
     */
    @Column(name = "login_ip")
    @ApiModelProperty("登陆IP")
    private String loginIp;

    /**
     * 注册城市
     */
    @Column(name = "login_city")
    @ApiModelProperty("注册城市")
    private String loginCity;

    /**
     * 注册设备
     */
    @Column(name = "reg_dev_key")
    @ApiModelProperty("注册设备")
    private String regDevKey;

    /**
     * 登录设备
     */
    @Column(name = "login_dev_key")
    @ApiModelProperty("登录设备")
    private String loginDevKey;

    /**
     * 登录时间
     */
    @Column(name = "last_login_time")
    @ApiModelProperty("登录时间")
    private Date lastLoginTime;

    /**
     * 注册时浏览器信息
     */
    @Column(name = "reg_user_agent")
    @ApiModelProperty("注册时浏览器信息")
    private String regUserAgent;

    /**
     * 登录时浏览器信息
     */
    @Column(name = "login_user_agent")
    @ApiModelProperty("登录时浏览器信息")
    private String loginUserAgent;

    /**
     * 登录次数
     */
    @Column(name = "login_times")
    @ApiModelProperty("登录次数")
    private Integer loginTimes;

    /**
     * 注册的版本号
     */
    @Column(name = "reg_app_version")
    @ApiModelProperty("注册的版本号")
    private String regAppVersion;

    /**
     * 当前版本号
     */
    @Column(name = "now_app_version")
    @ApiModelProperty("当前版本号")
    private String nowAppVersion;

    /**
     * 注册渠道编号
     */
    @Column(name = "reg_p")
    @ApiModelProperty("注册渠道编号")
    private Integer regP;

    /**
     * 登陆渠道编号
     */
    @Column(name = "login_p")
    @ApiModelProperty("登陆渠道编号")
    private Integer loginP;

    /**
     * 心跳时间
     */
    @Column(name = "heart_beat")
    @ApiModelProperty("心跳时间")
    private Date heartBeat;

    @Column(name = "dev_u_key")
    @ApiModelProperty("")
    private String devUKey;

    /**
     * 设备来源  1 安卓 2 ios 3 小程序
     */
    @Column(name = "dev_type")
    @ApiModelProperty("设备来源  1 安卓 2 ios 3 小程序")
    private Integer devType;

    /**
     * 是否在线(0：未在线1：在线)
     */
    @Column(name = "is_online")
    @ApiModelProperty("是否在线(0：未在线1：在线)")
    private Integer isOnline;

    /**
     * 上线下线时间
     */
    @Column(name = "online_time")
    @ApiModelProperty("上线下线时间")
    private Date onlineTime;

    /**
     * 登录的token
     */
    @Column(name = "session_key")
    @ApiModelProperty("登录的token")
    private String sessionKey;

    /**
     * 注册时填的邀请码
     */
    @Column(name = "invite_code")
    @ApiModelProperty("注册时填的邀请码")
    private String inviteCode;

    /**
     * 修改用户名状态 0-可修改 1-不可修改
     */
    @Column(name = "modify_account_status")
    @ApiModelProperty("修改用户名状态 0-可修改 1-不可修改")
    private String modifyAccountStatus;

    /**
     * 注册来源
     */
    @ApiModelProperty("注册来源")
    private String source;

    /**
     * MD5加密使用
     */
    @ApiModelProperty("MD5加密使用")
    private String salt;
    
    @Transient
    private List<GrantedAuthority> authorities;
    
    public void setAuthorities(List<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
    
    /**
     * 获取用户ID
     *
     * @return usr_id - 用户ID
     */
    public Long getUsrId() {
        return usrId;
    }

    /**
     * 设置用户ID
     *
     * @param usrId 用户ID
     */
    public void setUsrId(Long usrId) {
        this.usrId = usrId;
    }

    /**
     * 获取用户名
     *
     * @return usr_account - 用户名
     */
    public String getUsrAccount() {
        return usrAccount;
    }

    /**
     * 设置用户名
     *
     * @param usrAccount 用户名
     */
    public void setUsrAccount(String usrAccount) {
        this.usrAccount = usrAccount == null ? null : usrAccount.trim();
    }

    /**
     * 获取手机号码
     *
     * @return usr_mobile - 手机号码
     */
    public String getUsrMobile() {
        return usrMobile;
    }

    /**
     * 设置手机号码
     *
     * @param usrMobile 手机号码
     */
    public void setUsrMobile(String usrMobile) {
        this.usrMobile = usrMobile == null ? null : usrMobile.trim();
    }

    /**
     * 获取帐户密码
     *
     * @return usr_password - 帐户密码
     */
    public String getUsrPassword() {
        return usrPassword;
    }

    /**
     * 设置帐户密码
     *
     * @param usrPassword 帐户密码
     */
    public void setUsrPassword(String usrPassword) {
        this.usrPassword = usrPassword == null ? null : usrPassword.trim();
    }

    /**
     * 获取用户昵称
     *
     * @return usr_nick_name - 用户昵称
     */
    public String getUsrNickName() {
        return usrNickName;
    }

    /**
     * 设置用户昵称
     *
     * @param usrNickName 用户昵称
     */
    public void setUsrNickName(String usrNickName) {
        this.usrNickName = usrNickName == null ? null : usrNickName.trim();
    }

    /**
     * 获取头像
     *
     * @return head_pic - 头像
     */
    public String getHeadPic() {
        return headPic;
    }

    /**
     * 设置头像
     *
     * @param headPic 头像
     */
    public void setHeadPic(String headPic) {
        this.headPic = headPic == null ? null : headPic.trim();
    }

    /**
     * 获取用户等级 0-普通用户 1-认证企业用户
     *
     * @return group_id - 用户等级 0-普通用户 1-认证企业用户
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * 设置用户等级 0-普通用户 1-认证企业用户
     *
     * @param groupId 用户等级 0-普通用户 1-认证企业用户
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * 获取认证企业ID
     *
     * @return company_id - 认证企业ID
     */
    public Long getCompanyId() {
        return companyId;
    }

    /**
     * 设置认证企业ID
     *
     * @param companyId 认证企业ID
     */
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    /**
     * 获取账号状态(-2、被删除，-1，被冻结，1：正常，2：注销)
     *
     * @return status - 账号状态(-2、被删除，-1，被冻结，1：正常，2：注销)
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置账号状态(-2、被删除，-1，被冻结，1：正常，2：注销)
     *
     * @param status 账号状态(-2、被删除，-1，被冻结，1：正常，2：注销)
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取消息账号(0:未创建，1已创建)
     *
     * @return m_status - 消息账号(0:未创建，1已创建)
     */
    public Integer getmStatus() {
        return mStatus;
    }

    /**
     * 设置消息账号(0:未创建，1已创建)
     *
     * @param mStatus 消息账号(0:未创建，1已创建)
     */
    public void setmStatus(Integer mStatus) {
        this.mStatus = mStatus;
    }

    /**
     * 获取注册时间
     *
     * @return register_time - 注册时间
     */
    public Date getRegisterTime() {
        return registerTime;
    }

    /**
     * 设置注册时间
     *
     * @param registerTime 注册时间
     */
    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    /**
     * 获取注册IP
     *
     * @return register_ip - 注册IP
     */
    public String getRegisterIp() {
        return registerIp;
    }

    /**
     * 设置注册IP
     *
     * @param registerIp 注册IP
     */
    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp == null ? null : registerIp.trim();
    }

    /**
     * 获取登陆IP
     *
     * @return login_ip - 登陆IP
     */
    public String getLoginIp() {
        return loginIp;
    }

    /**
     * 设置登陆IP
     *
     * @param loginIp 登陆IP
     */
    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp == null ? null : loginIp.trim();
    }

    /**
     * 获取注册城市
     *
     * @return login_city - 注册城市
     */
    public String getLoginCity() {
        return loginCity;
    }

    /**
     * 设置注册城市
     *
     * @param loginCity 注册城市
     */
    public void setLoginCity(String loginCity) {
        this.loginCity = loginCity == null ? null : loginCity.trim();
    }

    /**
     * 获取注册设备
     *
     * @return reg_dev_key - 注册设备
     */
    public String getRegDevKey() {
        return regDevKey;
    }

    /**
     * 设置注册设备
     *
     * @param regDevKey 注册设备
     */
    public void setRegDevKey(String regDevKey) {
        this.regDevKey = regDevKey == null ? null : regDevKey.trim();
    }

    /**
     * 获取登录设备
     *
     * @return login_dev_key - 登录设备
     */
    public String getLoginDevKey() {
        return loginDevKey;
    }

    /**
     * 设置登录设备
     *
     * @param loginDevKey 登录设备
     */
    public void setLoginDevKey(String loginDevKey) {
        this.loginDevKey = loginDevKey == null ? null : loginDevKey.trim();
    }

    /**
     * 获取登录时间
     *
     * @return last_login_time - 登录时间
     */
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * 设置登录时间
     *
     * @param lastLoginTime 登录时间
     */
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    /**
     * 获取注册时浏览器信息
     *
     * @return reg_user_agent - 注册时浏览器信息
     */
    public String getRegUserAgent() {
        return regUserAgent;
    }

    /**
     * 设置注册时浏览器信息
     *
     * @param regUserAgent 注册时浏览器信息
     */
    public void setRegUserAgent(String regUserAgent) {
        this.regUserAgent = regUserAgent == null ? null : regUserAgent.trim();
    }

    /**
     * 获取登录时浏览器信息
     *
     * @return login_user_agent - 登录时浏览器信息
     */
    public String getLoginUserAgent() {
        return loginUserAgent;
    }

    /**
     * 设置登录时浏览器信息
     *
     * @param loginUserAgent 登录时浏览器信息
     */
    public void setLoginUserAgent(String loginUserAgent) {
        this.loginUserAgent = loginUserAgent == null ? null : loginUserAgent.trim();
    }

    /**
     * 获取登录次数
     *
     * @return login_times - 登录次数
     */
    public Integer getLoginTimes() {
        return loginTimes;
    }

    /**
     * 设置登录次数
     *
     * @param loginTimes 登录次数
     */
    public void setLoginTimes(Integer loginTimes) {
        this.loginTimes = loginTimes;
    }

    /**
     * 获取注册的版本号
     *
     * @return reg_app_version - 注册的版本号
     */
    public String getRegAppVersion() {
        return regAppVersion;
    }

    /**
     * 设置注册的版本号
     *
     * @param regAppVersion 注册的版本号
     */
    public void setRegAppVersion(String regAppVersion) {
        this.regAppVersion = regAppVersion == null ? null : regAppVersion.trim();
    }

    /**
     * 获取当前版本号
     *
     * @return now_app_version - 当前版本号
     */
    public String getNowAppVersion() {
        return nowAppVersion;
    }

    /**
     * 设置当前版本号
     *
     * @param nowAppVersion 当前版本号
     */
    public void setNowAppVersion(String nowAppVersion) {
        this.nowAppVersion = nowAppVersion == null ? null : nowAppVersion.trim();
    }

    /**
     * 获取注册渠道编号
     *
     * @return reg_p - 注册渠道编号
     */
    public Integer getRegP() {
        return regP;
    }

    /**
     * 设置注册渠道编号
     *
     * @param regP 注册渠道编号
     */
    public void setRegP(Integer regP) {
        this.regP = regP;
    }

    /**
     * 获取登陆渠道编号
     *
     * @return login_p - 登陆渠道编号
     */
    public Integer getLoginP() {
        return loginP;
    }

    /**
     * 设置登陆渠道编号
     *
     * @param loginP 登陆渠道编号
     */
    public void setLoginP(Integer loginP) {
        this.loginP = loginP;
    }

    /**
     * 获取心跳时间
     *
     * @return heart_beat - 心跳时间
     */
    public Date getHeartBeat() {
        return heartBeat;
    }

    /**
     * 设置心跳时间
     *
     * @param heartBeat 心跳时间
     */
    public void setHeartBeat(Date heartBeat) {
        this.heartBeat = heartBeat;
    }

    /**
     * @return dev_u_key
     */
    public String getDevUKey() {
        return devUKey;
    }

    /**
     * @param devUKey
     */
    public void setDevUKey(String devUKey) {
        this.devUKey = devUKey == null ? null : devUKey.trim();
    }

    /**
     * 获取设备来源  1 安卓 2 ios 3 小程序
     *
     * @return dev_type - 设备来源  1 安卓 2 ios 3 小程序
     */
    public Integer getDevType() {
        return devType;
    }

    /**
     * 设置设备来源  1 安卓 2 ios 3 小程序
     *
     * @param devType 设备来源  1 安卓 2 ios 3 小程序
     */
    public void setDevType(Integer devType) {
        this.devType = devType;
    }

    /**
     * 获取是否在线(0：未在线1：在线)
     *
     * @return is_online - 是否在线(0：未在线1：在线)
     */
    public Integer getIsOnline() {
        return isOnline;
    }

    /**
     * 设置是否在线(0：未在线1：在线)
     *
     * @param isOnline 是否在线(0：未在线1：在线)
     */
    public void setIsOnline(Integer isOnline) {
        this.isOnline = isOnline;
    }

    /**
     * 获取上线下线时间
     *
     * @return online_time - 上线下线时间
     */
    public Date getOnlineTime() {
        return onlineTime;
    }

    /**
     * 设置上线下线时间
     *
     * @param onlineTime 上线下线时间
     */
    public void setOnlineTime(Date onlineTime) {
        this.onlineTime = onlineTime;
    }

    /**
     * 获取登录的token
     *
     * @return session_key - 登录的token
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * 设置登录的token
     *
     * @param sessionKey 登录的token
     */
    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey == null ? null : sessionKey.trim();
    }

    /**
     * 获取注册时填的邀请码
     *
     * @return invite_code - 注册时填的邀请码
     */
    public String getInviteCode() {
        return inviteCode;
    }

    /**
     * 设置注册时填的邀请码
     *
     * @param inviteCode 注册时填的邀请码
     */
    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode == null ? null : inviteCode.trim();
    }

    /**
     * 获取修改用户名状态 0-可修改 1-不可修改
     *
     * @return modify_account_status - 修改用户名状态 0-可修改 1-不可修改
     */
    public String getModifyAccountStatus() {
        return modifyAccountStatus;
    }

    /**
     * 设置修改用户名状态 0-可修改 1-不可修改
     *
     * @param modifyAccountStatus 修改用户名状态 0-可修改 1-不可修改
     */
    public void setModifyAccountStatus(String modifyAccountStatus) {
        this.modifyAccountStatus = modifyAccountStatus == null ? null : modifyAccountStatus.trim();
    }

    /**
     * 获取注册来源
     *
     * @return source - 注册来源
     */
    public String getSource() {
        return source;
    }

    /**
     * 设置注册来源
     *
     * @param source 注册来源
     */
    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    /**
     * 获取MD5加密使用
     *
     * @return salt - MD5加密使用
     */
    public String getSalt() {
        return salt;
    }

    /**
     * 设置MD5加密使用
     *
     * @param salt MD5加密使用
     */
    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }
    
    @Override
    public String getPassword() {
        return null;
    }
    
    @Override
    public String getUsername() {
        return null;
    }
    
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    
    @Override
    public boolean isEnabled() {
        return true;
    }
}
