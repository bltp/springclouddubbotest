package com.yqr.gateway.service;

import com.yqr.common.result.R;
import com.yqr.gateway.persistence.mapper.UsrMapper;
import com.yqr.gateway.persistence.model.Usr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

/**
 * @program: springclouddubbotest
 * @description: security 用户接口
 * @author: Mr.Qian
 * @create: 2021-03-17 16:29
 **/
@Service
public class MyUserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UsrMapper usrMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Example example = new Example(Usr.class);
        example.createCriteria().andEqualTo("usrMobile", s);
        Usr usr = usrMapper.selectOneByExample(example);
        if (usr == null) {
            throw new UsernameNotFoundException("用户不存在");
        }
        return usr;
    }
}
