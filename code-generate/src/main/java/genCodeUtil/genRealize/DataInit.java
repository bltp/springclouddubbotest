package genCodeUtil.genRealize;

import genCodeUtil.genConfig.Column;
import genCodeUtil.genConfig.GenConfig;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能描述:
 * 〈构建模板所需数据〉
 *
 * @Author: Simon
 * @Date: 2020/4/7 11:01
 */
public class DataInit {

    GenConfig config;
    String author;
    String modelName;
    String tableName;
    String lowerModelName;
    List<Column> columns;

    public DataInit(GenConfig config, String author, String modelName, String tableName) {
        this.config = config;
        this.author = author;
        this.modelName = modelName;
        this.tableName = tableName;
        this.columns = BaseInit.listColumnByTableName(tableName);
        this.lowerModelName = BaseInit.toLowerCaseFirstOne(modelName);
    }

    /**
     * 功能描述:
     * 〈Manager〉
     *
     * @Author: Simon
     * @Date: 2020/4/7 11:30
     */
    public Map<String, Object> getDataMapInitForManager() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("date", new DateTime().toString("yyyy-MM-dd HH:mm:ss"));
        data.put("author", author);
        data.put("modelName", modelName);
        data.put("target_package_path", GenConfig.MANAGER_PACKAGE_PATH);
        data.put("mapper_package_path", GenConfig.MAPPER_PACKAGE_PATH);
        data.put("model_package_path", GenConfig.MODEL_PACKAGE_PATH);
        data.put("model_package_path", GenConfig.MODEL_PACKAGE_PATH);
        data.put("lowerModelName", lowerModelName);
        return data;
    }

    /**
     * 功能描述:
     * 〈Controller〉
     *
     * @Author: Simon
     * @Date: 2020/4/7 11:30
     */
    public Map<String, Object> getDataMapInitForController() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("date", new DateTime().toString("yyyy-MM-dd HH:mm:ss"));
        data.put("author", author);
        data.put("modelName", modelName);
        data.put("target_package_path", GenConfig.CONTROLLER_PACKAGE_PATH);
        data.put("mapper_package_path", GenConfig.MAPPER_PACKAGE_PATH);
        data.put("model_package_path", GenConfig.MODEL_PACKAGE_PATH);
        data.put("service_package_path", GenConfig.SERVICE_PACKAGE_PATH);
        data.put("lowerModelName", lowerModelName);
        data.put("columns", columns);
        return data;
    }

    /**
     * 功能描述:
     * 〈Service〉
     *
     * @Author: Simon
     * @Date: 2020/4/7 11:30
     */
    public Map<String, Object> getDataMapInitForService() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("date", new DateTime().toString("yyyy-MM-dd HH:mm:ss"));
        data.put("author", author);
        data.put("modelName", modelName);
        data.put("target_package_path", GenConfig.SERVICE_PACKAGE_PATH);
        data.put("mapper_package_path", GenConfig.MAPPER_PACKAGE_PATH);
        data.put("model_package_path", GenConfig.MODEL_PACKAGE_PATH);
        data.put("srvice_base_package_path", GenConfig.SERVICE_PACKAGE_PATH);
        data.put("manager_package_path", GenConfig.MANAGER_PACKAGE_PATH);
        data.put("lowerModelName", lowerModelName);
        data.put("columns", columns);
        return data;
    }
    
    /**
     * 功能描述:
     * 〈Service〉
     *
     * @Author: Simon
     * @Date: 2020/4/7 11:30
     */
    public Map<String, Object> getDataMapInitForServiceImpl() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("date", new DateTime().toString("yyyy-MM-dd HH:mm:ss"));
        data.put("author", author);
        data.put("modelName", modelName);
        data.put("target_package_path", GenConfig.SERVICE_PACKAGE_IMPL_PATH);
        data.put("srvice_base_package_path", GenConfig.SERVICE_PACKAGE_PATH);
        data.put("mapper_package_path", GenConfig.MAPPER_PACKAGE_PATH);
        data.put("model_package_path", GenConfig.MODEL_PACKAGE_PATH);
        data.put("manager_package_path", GenConfig.MANAGER_PACKAGE_PATH);
        data.put("lowerModelName", lowerModelName);
        data.put("columns", columns);
        return data;
    }

}
