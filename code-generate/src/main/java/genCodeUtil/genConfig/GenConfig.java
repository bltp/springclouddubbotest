package genCodeUtil.genConfig;


import genCodeUtil.util.PropertiesUtils;
import genCodeUtil.util.config.ConfigurableProcessor;
import genCodeUtil.util.config.Property;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * 功能描述:
 * 〈配置类〉
 *
 * @Author: Simon
 * @Date: 2020/4/7 10:06
 */
@Configuration
public class GenConfig {

    /**
     * 数据源相关配置信息
     */
    @Property(key = "db.jdbc.url", defaultValue = "")
    public static String JDBC_URL;
    @Property(key = "db.jdbc.username", defaultValue = "")
    public static String JDBC_USERNAME;
    @Property(key = "db.jdbc.password", defaultValue = "")
    public static String JDBC_PASSWORD;
    @Property(key = "db.jdbc.classname", defaultValue = "")
    public static String JDBC_DRIVER_CLASS_NAME;

    /**
     * 模板目录-工程下绝对路径
     */
    @Property(key = "gen.template.path", defaultValue = "")
    public static String TEMPLATE_PATH;
    /**
     * Controller目录-工程下绝对路径
     */
    @Property(key = "gen.controller.path", defaultValue = "")
    public static String CONTROLLER_PATH;
    /**
     * Manager目录-工程下绝对路径
     */
    @Property(key = "gen.manage.path", defaultValue = "")
    public static String MANAGER_PATH;
    /**
     * Service目录-工程下绝对路径
     */
    @Property(key = "gen.service.path", defaultValue = "")
    public static String SERVICE_PATH;
    /**
     * Service目录-工程下绝对路径
     */
    @Property(key = "gen.service.impl.path", defaultValue = "")
    public static String SERVICE_IMPL_PATH;
    /**
     * resources目录-工程下绝对路径
     */
    @Property(key = "gen.resources.path", defaultValue = "")
    public static String RESOURCES_PATH;
    /**
     * java目录-工程下绝对路径
     */
    @Property(key = "gen.java.path", defaultValue = "")
    public static String JAVA_PATH;
    /**
     * 通用Mapper路径
     */
    @Property(key = "gen.mapper.interface.reference", defaultValue = "")
    public static String MAPPER_INTERFACE_REFERENCE;
    /**
     * XML包路径
     */
    @Property(key = "gen.xml.package.path", defaultValue = "")
    public static String XML_PACKAGE_PATH;
    /**
     * Manager包路径
     */
    @Property(key = "gen.manager.package.path", defaultValue = "")
    public static String MANAGER_PACKAGE_PATH;
    /**
     * Mapper包路径
     */
    @Property(key = "gen.mapper.package.path", defaultValue = "")
    public static String MAPPER_PACKAGE_PATH;
    /**
     * Model包路径
     */
    @Property(key = "gen.model.package.path", defaultValue = "")
    public static String MODEL_PACKAGE_PATH;
    /**
     * Service包路径
     */
    @Property(key = "gen.service.package.path", defaultValue = "")
    public static String SERVICE_PACKAGE_PATH;
    /**
     * ServiceImpl包路径
     */
    @Property(key = "gen.service.impl.package.path", defaultValue = "")
    public static String SERVICE_PACKAGE_IMPL_PATH;
    /**
     * Controller包路径
     */
    @Property(key = "gen.controller.package.path", defaultValue = "")
    public static String CONTROLLER_PACKAGE_PATH;
    /**
     * Controller模板名称
     */
    public static final String TEMPLATE_NAME_CONTROLLER = "controllerGen.ftl";
    /**
     * Service模板名称
     */
    public static final String TEMPLATE_NAME_SERVICE = "serviceGen.ftl";
    /**
     * ServiceImpl模板名称
     */
    public static final String TEMPLATE_NAME_SERVICE_IMPL = "serviceImplGen.ftl";
    /**
     * Manager模板名称
     */
    public static final String TEMPLATE_NAME_MANAGER = "managerGen.ftl";
    /**
     * 工程路径
     */
    public static String PROJECT_PATH = System.getProperty("user.dir");


}
