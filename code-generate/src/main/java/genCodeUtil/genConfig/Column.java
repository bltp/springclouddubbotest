package genCodeUtil.genConfig;

import lombok.Data;

/**
 * @author : Simon
 * @date : 2020/4/9
 */
@Data
public class Column {

    //字段名称
    private String name;
    //字段注释
    private String annotation;
    //字段长度
    private Integer length;
    //字段类型
    private String type;
    //字段Java类型
    private String javaType;
    //字段是否可以为空
    private String isNullable;
    //Java对象名称
    private String javaName;
    //Java对象名称(首字母大写)
    private String fristMajusculeJavaType;
}
