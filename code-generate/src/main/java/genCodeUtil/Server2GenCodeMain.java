package genCodeUtil;


import genCodeUtil.genConfig.GenConfig;
import genCodeUtil.genRealize.GenRealize;
import genCodeUtil.util.PropertiesUtils;
import genCodeUtil.util.config.ConfigurableProcessor;

import java.util.Properties;

/**
 * 代码生成器启动类
 */
public class Server2GenCodeMain {
    static {
        try {
            Properties[] props = new Properties[1];
            props[0] = PropertiesUtils.loadClassLoader("server2-config.properties");
            ConfigurableProcessor.process(GenConfig.class, props);
            
        } catch (Exception e) {
            throw new Error("Can't load loginserver configuration", e);
        }
    }
    public static void main(String[] args) {
        try {
            Properties[] props = new Properties[1];
            props[0] = PropertiesUtils.loadClassLoader("server2-config.properties");
            ConfigurableProcessor.process(GenConfig.class, props);
        
        } catch (Exception e) {
            throw new Error("Can't load loginserver configuration", e);
        }
        //表名
        String tableName = "t_student3";
        //实体类名称
        String modelName = "Student3";
        //生成人
        String author = "Simon";

        /**
         * 生成代码
         * 一键生成 mapper,model,xml,controller,service,manager
         */
        GenRealize genRealize = new GenRealize(modelName, tableName, author);
        //想定制化生成在下面这个方法中注释
        genRealize.genCodeQueue();
    }

}
