package ${target_package_path};

import ${model_package_path}.${modelName};
import ${service_package_path}.${modelName}Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yqr.common.result.PageInfoWrapper;
import com.yqr.common.result.R;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

/**
* 接口说明: <>
* @author : ${author}
* @date : ${date}
*/
@RestController
@RequestMapping("${modelName}Controller")
@Api(tags = "[请更改接口说明]")
public class ${modelName}Controller {

    @Autowired
    private ${modelName}Service ${lowerModelName}Service;

    /**
    * 分页查询${modelName}
    */
    @GetMapping(value = "query${modelName}ForPage")
    @ApiOperation(value = "分页查询${modelName}", httpMethod = "GET", notes = "")
    public R<PageInfoWrapper<${modelName}>> query${modelName}ForPage(
            @ApiParam(required = false, name = "pageNum", value = "页号") @RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
            @ApiParam(required = false, name = "pageSize", value = "每页显示多少条") @RequestParam(value = "pageSize", defaultValue = "7", required = false) Integer pageSize
    ) {
        return ${lowerModelName}Service.query${modelName}ForPage(pageNum, pageSize);
    }

    /**
    * 根据主键查询${modelName}
    */
    @GetMapping(value = "get${modelName}ByPrimaryKey")
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "")
    public R<${modelName}> get${modelName}ByPrimaryKey(
        @ApiParam(required = true, name = "id", value = "主键参数") @RequestParam(value = "id", defaultValue = "", required = true) Long id
        ) {
        return ${lowerModelName}Service.get${modelName}ByPrimaryKey(id);
        }

    /**
    * 新增${modelName}
    */
    @PostMapping(value = "insert${modelName}")
    @ApiOperation(value = "新增${modelName}", httpMethod = "POST", notes = "")
    @ApiOperationSupport(author = "Simon",includeParameters = {""})
    public R insert${modelName}(${modelName} ${lowerModelName}) {
        return ${lowerModelName}Service.insert${modelName}(${lowerModelName});
    }

    /**
    * 编辑${modelName}
    */
    @PostMapping(value = "update${modelName}")
    @ApiOperation(value = "编辑${modelName}", httpMethod = "POST", notes = "")
    @ApiOperationSupport(author = "Simon",includeParameters = {""})
    public R update${modelName}(${modelName} ${lowerModelName}) {
        return ${lowerModelName}Service.update${modelName}(${lowerModelName});
    }


}
