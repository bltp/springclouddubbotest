package ${target_package_path};
import ${srvice_base_package_path}.${modelName}Service;
import ${model_package_path}.${modelName};
import ${manager_package_path}.${modelName}Manager;
import com.sohu.idcenter.IdWorker;
import com.yqr.common.result.PageInfoWrapper;
import com.yqr.common.result.R;
import com.yqr.common.util.ValidateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.math.BigDecimal;
import com.yqr.service.BaseServiceImpl;
import java.util.*;


/**
* 接口实现说明: <>
* @author : ${author}
* @date : ${date}
*/
@Service
@Slf4j
public class ${modelName}Service{

    @Autowired
    private ${modelName}Manager ${lowerModelName}Manager;
    @Autowired
    private IdWorker idWorker;

    /**
    * 分页查询${modelName}
    */

    public R<PageInfoWrapper<${modelName}>> query${modelName}ForPage(Integer pageNum,Integer pageSize) {
        PageHelper.startPage(null == pageNum ? 1 : pageNum, null == pageSize ? 10 : pageSize);
        List<${modelName}> list = ${lowerModelName}Manager.selectAll();
        PageInfo<${modelName}> pageInfo = new PageInfo<>(list);
        return R.data(PageInfoWrapper.build(pageInfo, list));
    }

    /**
    * 根据主键查询${modelName}
    */

    public R<${modelName}> get${modelName}ByPrimaryKey(Long id){
        if(ValidateUtils.isNull(id)){
            return R.fail("查询参数不能为空");
        }
        ${modelName} ${lowerModelName} = ${lowerModelName}Manager.selectByPrimaryKey(id);
        if(ValidateUtils.isNull(${lowerModelName})){
            return R.fail("查询不到要编辑的元素");
        }
        return R.data(${lowerModelName});
    }

    /**
    * 新增${modelName}
    */

    public R insert${modelName}(${modelName} ${lowerModelName}){
<#list columns as value>
    <#if value.javaName!="id">
        <#if value.isNullable=="true">
            <#if value.type=='VARCHAR' || value.type=='CHAR'>
        if (!ValidateUtils.isNull(${lowerModelName}.get${value.fristMajusculeJavaType}()) && ValidateUtils.exceedLength(${lowerModelName}.get${value.fristMajusculeJavaType}(), ${value.length?string('#.##')})) {
        return R.fail("${value.javaName}参数长度必须在 1-${value.length?string('#.##')} 之间!");
        }
            </#if>
        </#if>
        <#if value.isNullable!="true">
            <#if value.type=='VARCHAR' || value.type=='CHAR'>
        if (ValidateUtils.isNull(${lowerModelName}.get${value.fristMajusculeJavaType}()) || ValidateUtils.exceedLength(${lowerModelName}.get${value.fristMajusculeJavaType}(), ${value.length?string('#.##')})) {
        return R.fail("${value.javaName}参数长度必须在 1-${value.length?string('#.##')} 之间!");
        }
            </#if>
            <#if value.type=='BIGINT' || value.type=='INT'>
        if (ValidateUtils.isNull(${lowerModelName}.get${value.fristMajusculeJavaType}())) {
        return R.fail("${value.javaName}参数必填!");
        }
            </#if>
        </#if>
    </#if>
</#list>

        ${lowerModelName}.setId(idWorker.getId());
        ${lowerModelName}Manager.insert(${lowerModelName});
        return R.success("ok");
    }

    /**
    * 编辑${modelName}
    */

    public R update${modelName}(${modelName} ${lowerModelName}){
<#list columns as value>
    <#if value.javaName!="id">
        <#if value.isNullable=="true">
            <#if value.type=='VARCHAR' || value.type=='CHAR'>
        if (!ValidateUtils.isNull(${lowerModelName}.get${value.fristMajusculeJavaType}()) && ValidateUtils.exceedLength(${lowerModelName}.get${value.fristMajusculeJavaType}(), ${value.length?string('#.##')})) {
            return R.fail("${value.javaName}参数长度必须在 1-${value.length?string('#.##')} 之间!");
        }
            </#if>
        </#if>
        <#if value.isNullable!="true">
            <#if value.type=='VARCHAR' || value.type=='CHAR'>
        if (ValidateUtils.isNull(${lowerModelName}.get${value.fristMajusculeJavaType}()) || ValidateUtils.exceedLength(${lowerModelName}.get${value.fristMajusculeJavaType}(), ${value.length?string('#.##')})) {
            return R.fail("${value.javaName}参数长度必须在 1-${value.length?string('#.##')} 之间!");
        }
            </#if>
            <#if value.type=='BIGINT' || value.type=='INT'>
        if (ValidateUtils.isNull(${lowerModelName}.get${value.fristMajusculeJavaType}())) {
            return R.fail("${value.javaName}参数必填!");
        }
            </#if>
            <#if value.type=='DATE' || value.type=='DATETIME'>
        if (ValidateUtils.isNull(${lowerModelName}.get${value.fristMajusculeJavaType}())) {
        return R.fail("${value.javaName}参数必填!");
        }
            </#if>
        </#if>

    </#if>
</#list>
        //在这边写编辑逻辑

        ${lowerModelName}Manager.updateByPrimaryKeySelective(${lowerModelName});
            return R.success("ok");
        }

}
