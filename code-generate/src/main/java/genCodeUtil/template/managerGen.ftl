package ${target_package_path};

import ${mapper_package_path}.${modelName}Mapper;
import ${model_package_path}.${modelName};
import com.yqr.manage.BaseManage;
import com.yqr.mapper.MyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;
import com.yqr.common.util.ValidateUtils;

import java.util.*;
/**
* @author : ${author}
* @date : ${date}
*/

@Component
public class ${modelName}Manager extends BaseManage<${modelName}> {

    @Autowired
    private ${modelName}Mapper ${lowerModelName}Mapper;

    @Override
    protected MyMapper<${modelName}> getMapper() {
        return ${lowerModelName}Mapper;
    }
}
