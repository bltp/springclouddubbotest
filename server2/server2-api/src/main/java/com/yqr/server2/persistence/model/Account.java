package com.yqr.server2.persistence.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;
import lombok.ToString;

@ToString
@ApiModel("")
@Table(name = "tbl_account")
public class Account {
    @Id
    @GeneratedValue(generator = "JDBC")
    @ApiModelProperty("")
    private Long id;

    @ApiModelProperty("")
    private Integer balance;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return balance
     */
    public Integer getBalance() {
        return balance;
    }

    /**
     * @param balance
     */
    public void setBalance(Integer balance) {
        this.balance = balance;
    }
}