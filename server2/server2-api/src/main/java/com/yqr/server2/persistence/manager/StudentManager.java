package com.yqr.server2.persistence.manager;

import com.yqr.server2.persistence.mapper.StudentMapper;
import com.yqr.server2.persistence.model.Student;
import com.yqr.manage.BaseManage;
import com.yqr.mapper.MyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;
import com.yqr.common.util.ValidateUtils;

import java.util.*;
/**
* @author : Simon
* @date : 2021-05-25 08:57:27
*/

@Component
public class StudentManager extends BaseManage<Student> {

    @Autowired
    private StudentMapper studentMapper;

    @Override
    protected MyMapper<Student> getMapper() {
        return studentMapper;
    }
}
