package com.yqr.server2;

import com.yqr.server2.persistence.model.Account;
import com.yqr.server2.persistence.model.Repo;
import com.yqr.service.BaseService;

public interface RepoService extends BaseService<Repo> {
    
    /**
     * 扣除存储数量
     */
    void deduct(String productCode, int count);
}
