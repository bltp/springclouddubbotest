package com.yqr.server2.persistence.model;

import com.yqr.common.enums.CommonEnums;
import com.yqr.server2.persistence.enums.GradeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;
import org.apache.ibatis.type.JdbcType;
import tk.mybatis.mapper.annotation.ColumnType;

@Data
@ApiModel("")
@Table(name = "t_student")
public class Student implements Serializable {
    /**
     * id
     */
    @Id
    @Column(name = "id")
    @ApiModelProperty("id")
    private Long id;

    /**
     * 姓名
     */
    @Column(name = "name")
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 性别：F：男 M：女
     */
    @Column(name = "gender")
    @ApiModelProperty("性别：F：男 M：女")
    @ColumnType(jdbcType = JdbcType.CHAR)
    private CommonEnums.Gender gender;

    /**
     * 创建人id
     */
    @Column(name = "created_by")
    @ApiModelProperty("创建人id")
    private Long createdBy;

    /**
     * 创建时间
     */
    @Column(name = "created_time")
    @ApiModelProperty("创建时间")
    private Date createdTime;

    /**
     * 修改人id
     */
    @Column(name = "modified_by")
    @ApiModelProperty("修改人id")
    private Long modifiedBy;

    /**
     * 修改时间
     */
    @Column(name = "modified_time")
    @ApiModelProperty("修改时间")
    private Date modifiedTime;

    /**
     * 年级 一年级：G1 二年级：G2 三年级：G3
     */
    @Column(name = "grade")
    @ColumnType(jdbcType = JdbcType.VARCHAR)
    @ApiModelProperty("年级 一年级：G1 二年级：G2 三年级：G3")
    private GradeEnum grade;

    private static final long serialVersionUID = 1L;
}
