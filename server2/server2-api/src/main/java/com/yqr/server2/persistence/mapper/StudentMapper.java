package com.yqr.server2.persistence.mapper;

import com.yqr.mapper.MyMapper;
import com.yqr.server2.persistence.model.Student;

public interface StudentMapper extends MyMapper<Student> {
}