package com.yqr.server2;

import com.yqr.server2.persistence.model.Order;
import com.yqr.service.BaseService;

public interface OrderService extends BaseService<Order> {

}
