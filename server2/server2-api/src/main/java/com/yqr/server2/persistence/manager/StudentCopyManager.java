package com.yqr.server2.persistence.manager;

import com.yqr.server2.persistence.mapper.StudentCopyMapper;
import com.yqr.server2.persistence.model.StudentCopy;
import com.yqr.manage.BaseManage;
import com.yqr.mapper.MyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;
import com.yqr.common.util.ValidateUtils;

import java.util.*;
/**
* @author : Simon
* @date : 2021-05-25 08:56:48
*/

@Component
public class StudentCopyManager extends BaseManage<StudentCopy> {

    @Autowired
    private StudentCopyMapper studentCopyMapper;

    @Override
    protected MyMapper<StudentCopy> getMapper() {
        return studentCopyMapper;
    }
}
