package com.yqr.server2.persistence.mapper;

import java.util.Map;

import com.yqr.mapper.MyMapper;
import com.yqr.server2.persistence.model.Repo;

public interface RepoMapper extends MyMapper<Repo> {
    int deduct(Map<String, Object> map);
}
