package com.yqr.server2;

import com.yqr.server2.persistence.model.Account;
import com.yqr.service.BaseService;

public interface AccountService extends BaseService<Account> {

}
