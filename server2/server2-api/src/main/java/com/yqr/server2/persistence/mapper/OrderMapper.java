package com.yqr.server2.persistence.mapper;

import com.yqr.mapper.MyMapper;
import com.yqr.server2.persistence.model.Order;

public interface OrderMapper extends MyMapper<Order> {
}