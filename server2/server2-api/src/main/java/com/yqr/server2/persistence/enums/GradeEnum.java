package com.yqr.server2.persistence.enums;

import com.yqr.common.enums.IBaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum GradeEnum implements IBaseEnum {
    
    G1("一年級"),G2("二年級"),G3("三年級");
    
    @Getter
    private String desc;
}
