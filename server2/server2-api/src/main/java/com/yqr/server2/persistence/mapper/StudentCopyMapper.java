package com.yqr.server2.persistence.mapper;

import com.yqr.mapper.MyMapper;
import com.yqr.server2.persistence.model.StudentCopy;

public interface StudentCopyMapper extends MyMapper<StudentCopy> {
}