package com.yqr.server2.persistence.manager;

import com.yqr.server2.persistence.mapper.Student3Mapper;
import com.yqr.server2.persistence.model.Student3;
import com.yqr.manage.BaseManage;
import com.yqr.mapper.MyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;
import com.yqr.common.util.ValidateUtils;

import java.util.*;
/**
* @author : Simon
* @date : 2021-05-25 09:31:33
*/

@Component
public class Student3Manager extends BaseManage<Student3> {

    @Autowired
    private Student3Mapper student3Mapper;

    @Override
    protected MyMapper<Student3> getMapper() {
        return student3Mapper;
    }
}
