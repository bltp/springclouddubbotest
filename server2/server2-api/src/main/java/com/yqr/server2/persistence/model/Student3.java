package com.yqr.server2.persistence.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@ApiModel("")
@Table(name = "t_student3")
public class Student3 implements Serializable {
    /**
     * id
     */
    @Id
    @Column(name = "id")
    @ApiModelProperty("id")
    private Long id;

    /**
     * 姓名
     */
    @Column(name = "name")
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 性别：F：男 M：女
     */
    @Column(name = "gender")
    @ApiModelProperty("性别：F：男 M：女")
    private String gender;

    /**
     * 创建人id
     */
    @Column(name = "created_by")
    @ApiModelProperty("创建人id")
    private Long createdBy;

    /**
     * 创建时间
     */
    @Column(name = "created_time")
    @ApiModelProperty("创建时间")
    private Date createdTime;

    /**
     * 修改人id
     */
    @Column(name = "modified_by")
    @ApiModelProperty("修改人id")
    private Long modifiedBy;

    /**
     * 修改时间
     */
    @Column(name = "modified_time")
    @ApiModelProperty("修改时间")
    private Date modifiedTime;

    /**
     * 年级 一年级：G1 二年级：G2 三年级：G3
     */
    @Column(name = "grade")
    @ApiModelProperty("年级 一年级：G1 二年级：G2 三年级：G3")
    private String grade;

    private static final long serialVersionUID = 1L;
}