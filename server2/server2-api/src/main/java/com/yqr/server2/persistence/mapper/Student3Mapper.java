package com.yqr.server2.persistence.mapper;

import com.yqr.mapper.MyMapper;
import com.yqr.server2.persistence.model.Student3;

public interface Student3Mapper extends MyMapper<Student3> {
}