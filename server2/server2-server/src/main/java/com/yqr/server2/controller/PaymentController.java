package com.yqr.server2.controller;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.yqr.common.result.ApiDataType;
import com.yqr.common.result.ApiParamType;
import com.yqr.common.result.R;
import com.yqr.common.util.ValidateUtils;
import com.yqr.oss.AliOSSKit;
import com.yqr.server2.dto.PaymentHtmlDto;
import freemarker.template.Configuration;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: springclouddubbotest
 * @description: 微票宝兑付记录生成html
 * @author: Tim
 * @create: 2021-05-26 15:13
 **/
@Api(tags="微票宝兑付记录生成html")
@RestController
public class PaymentController {
    @Autowired
    private Configuration cfg;
    
    @PostMapping(value = "generateHtml")
    @ApiOperation(value = "微票宝兑付记录生成html", httpMethod = "POST", notes = "")
 
    @ApiOperationSupport(author = "Simon")
    public R<PaymentHtmlDto> generateHtml(
             String companyIds
            ){
        if(ValidateUtils.isNull(companyIds)){
            return R.fail("请出入companyIds参数");
        }
        String[] companys=companyIds.split(",");
        try {
            File file = File.createTempFile("payment",".html");
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            Map<String, List<String>> map=new HashMap<>();
            map.put("data", Arrays.asList(companys));
            cfg.getTemplate("payment.ftl").process(map, new FileWriter(file));
            AliOSSKit.uploadFile(file,"apk/payment.html");
            System.out.println("paymnet.html" + "生成成功");
        } catch (Exception e) {
            throw new RuntimeException("paymnet.html" + "生成失败!", e);
        }
        PaymentHtmlDto paymentHtmlDto=new PaymentHtmlDto();
        paymentHtmlDto.setPaymentHtmlUrl(AliOSSKit.getAbsoluteUrl("apk/payment.html"));
        return R.data(paymentHtmlDto);
    }
}
