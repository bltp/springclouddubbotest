package com.yqr.server2.controller;

import com.yqr.server2.persistence.model.Student;
import com.yqr.server2.service.StudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yqr.common.result.PageInfoWrapper;
import com.yqr.common.result.R;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

/**
* 接口说明: <>
* @author : Simon
* @date : 2021-05-25 08:57:28
*/
@RestController
@RequestMapping("StudentController")
@Api(tags = "学生管理接口")
public class StudentController {

    @Autowired
    private StudentService studentService;

    /**
    * 分页查询Student
    */
    @GetMapping(value = "queryStudentForPage")
    @ApiOperation(value = "分页查询Student", httpMethod = "GET", notes = "")
    public R<PageInfoWrapper<Student>> queryStudentForPage(
            @ApiParam(required = false, name = "pageNum", value = "页号") @RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
            @ApiParam(required = false, name = "pageSize", value = "每页显示多少条") @RequestParam(value = "pageSize", defaultValue = "7", required = false) Integer pageSize
    ) {
        return studentService.queryStudentForPage(pageNum, pageSize);
    }

    /**
    * 根据主键查询Student
    */
    @GetMapping(value = "getStudentByPrimaryKey")
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "")
    public R<Student> getStudentByPrimaryKey(
        @ApiParam(required = true, name = "id", value = "主键参数") @RequestParam(value = "id", defaultValue = "", required = true) Long id
        ) {
        return studentService.getStudentByPrimaryKey(id);
        }

    /**
    * 新增Student
    */
    @PostMapping(value = "insertStudent")
    @ApiOperation(value = "新增Student", httpMethod = "POST", notes = "")
    @ApiOperationSupport(author = "Simon",includeParameters = {"name","gender","grade"})
    public R insertStudent(Student student) {
        return studentService.insertStudent(student);
    }

    /**
    * 编辑Student
    */
    @PostMapping(value = "updateStudent")
    @ApiOperation(value = "编辑Student", httpMethod = "POST", notes = "")
    @ApiOperationSupport(author = "Simon",includeParameters = {"id","name","gender","grade"})
    public R updateStudent(Student student) {
        return studentService.updateStudent(student);
    }


}
