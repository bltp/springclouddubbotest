package com.yqr.server2.dubbo;

import com.alibaba.dubbo.config.annotation.Service;
        import com.baomidou.dynamic.datasource.annotation.DS;
        import com.yqr.server2.AccountService;
        import com.yqr.server2.persistence.model.Account;
        import com.yqr.service.BaseServiceImpl;

/**
 * @program: springclouddubbotest
 * @description: 账号实现
 * @author: Mr.Qian
 * @create: 2021-04-15 23:04
 **/
@Service
@DS("account")
public class AccountServiceImpl extends BaseServiceImpl<Account> implements AccountService {


}
