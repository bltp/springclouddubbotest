package com.yqr.server2.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @program: springclouddubbotest
 * @description: 返回微票宝兑付记录封装html
 * @author: Tim
 * @create: 2021-05-26 16:10
 **/
@Data
@ApiModel("返回微票宝兑付记录封装html")
public class PaymentHtmlDto {
    @ApiModelProperty("html地址")
    private String paymentHtmlUrl;
}
