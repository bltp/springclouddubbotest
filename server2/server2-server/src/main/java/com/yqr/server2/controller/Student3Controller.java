package com.yqr.server2.controller;

import com.yqr.server2.persistence.model.Student3;
import com.yqr.server2.service.Student3Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yqr.common.result.PageInfoWrapper;
import com.yqr.common.result.R;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

/**
* 接口说明: <>
* @author : Simon
* @date : 2021-05-25 09:31:34
*/
@RestController
@RequestMapping("Student3Controller")
@Api(tags = "[请更改接口说明]")
public class Student3Controller {

    @Autowired
    private Student3Service student3Service;

    /**
    * 分页查询Student3
    */
    @GetMapping(value = "queryStudent3ForPage")
    @ApiOperation(value = "分页查询Student3", httpMethod = "GET", notes = "")
    public R<PageInfoWrapper<Student3>> queryStudent3ForPage(
            @ApiParam(required = false, name = "pageNum", value = "页号") @RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
            @ApiParam(required = false, name = "pageSize", value = "每页显示多少条") @RequestParam(value = "pageSize", defaultValue = "7", required = false) Integer pageSize
    ) {
        return student3Service.queryStudent3ForPage(pageNum, pageSize);
    }

    /**
    * 根据主键查询Student3
    */
    @GetMapping(value = "getStudent3ByPrimaryKey")
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "")
    public R<Student3> getStudent3ByPrimaryKey(
        @ApiParam(required = true, name = "id", value = "主键参数") @RequestParam(value = "id", defaultValue = "", required = true) Long id
        ) {
        return student3Service.getStudent3ByPrimaryKey(id);
        }

    /**
    * 新增Student3
    */
    @PostMapping(value = "insertStudent3")
    @ApiOperation(value = "新增Student3", httpMethod = "POST", notes = "")
    @ApiOperationSupport(author = "Simon",includeParameters = {"name","gender","grade"})
    public R insertStudent3(Student3 student3) {
        return student3Service.insertStudent3(student3);
    }

    /**
    * 编辑Student3
    */
    @PostMapping(value = "updateStudent3")
    @ApiOperation(value = "编辑Student3", httpMethod = "POST", notes = "")
    @ApiOperationSupport(author = "Simon",includeParameters = {"id","name","gender","grade"})
    public R updateStudent3(Student3 student3) {
        return student3Service.updateStudent3(student3);
    }


}
