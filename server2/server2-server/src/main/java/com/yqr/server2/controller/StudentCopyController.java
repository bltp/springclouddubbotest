package com.yqr.server2.controller;

import com.yqr.server2.persistence.model.StudentCopy;
import com.yqr.server2.service.StudentCopyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yqr.common.result.PageInfoWrapper;
import com.yqr.common.result.R;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;

/**
* 接口说明: <>
* @author : Simon
* @date : 2021-05-25 08:56:48
*/
@RestController
@RequestMapping("StudentCopyController")
@Api(tags = "学生copy管理接口")
public class StudentCopyController {

    @Autowired
    private StudentCopyService studentCopyService;

    /**
    * 分页查询StudentCopy
    */
    @GetMapping(value = "queryStudentCopyForPage")
    @ApiOperation(value = "分页查询StudentCopy", httpMethod = "GET", notes = "")
    public R<PageInfoWrapper<StudentCopy>> queryStudentCopyForPage(
            @ApiParam(required = false, name = "pageNum", value = "页号") @RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
            @ApiParam(required = false, name = "pageSize", value = "每页显示多少条") @RequestParam(value = "pageSize", defaultValue = "7", required = false) Integer pageSize
    ) {
        return studentCopyService.queryStudentCopyForPage(pageNum, pageSize);
    }

    /**
    * 根据主键查询StudentCopy
    */
    @GetMapping(value = "getStudentCopyByPrimaryKey")
    @ApiOperation(value = "根据主键查询", httpMethod = "GET", notes = "")
    public R<StudentCopy> getStudentCopyByPrimaryKey(
        @ApiParam(required = true, name = "id", value = "主键参数") @RequestParam(value = "id", defaultValue = "", required = true) Long id
        ) {
        return studentCopyService.getStudentCopyByPrimaryKey(id);
        }

    /**
    * 新增StudentCopy
    */
    @PostMapping(value = "insertStudentCopy")
    @ApiOperation(value = "新增StudentCopy", httpMethod = "POST", notes = "")
    @ApiOperationSupport(author = "Simon",includeParameters = {"name","gender","grade"})
    public R insertStudentCopy(StudentCopy studentCopy) {
        return studentCopyService.insertStudentCopy(studentCopy);
    }

    /**
    * 编辑StudentCopy
    */
    @PostMapping(value = "updateStudentCopy")
    @ApiOperation(value = "编辑StudentCopy", httpMethod = "POST", notes = "")
    @ApiOperationSupport(author = "Simon",includeParameters = {"id","name","gender","grade"})
    public R updateStudentCopy(StudentCopy studentCopy) {
        return studentCopyService.updateStudentCopy(studentCopy);
    }


}
