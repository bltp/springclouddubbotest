package com.yqr.server2.dubbo;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.yqr.common.exception.ServiceException;
import com.yqr.server2.RepoService;
import com.yqr.server2.persistence.mapper.RepoMapper;
import com.yqr.server2.persistence.model.Order;
import com.yqr.server2.persistence.model.Repo;
import com.alibaba.dubbo.config.annotation.Service;
import com.yqr.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: springclouddubbotest
 * @description: 庫存服務
 * @author: Mr.Qian
 * @create: 2021-03-24 11:55
 **/
@Service
@DS("repo")
public class RepoServiceImpl extends BaseServiceImpl<Repo>  implements RepoService {
    @Autowired
    private RepoMapper repoMapper;
    
    @Override
    public void deduct(String productCode, int count) {
        Map<String,Object> map = new HashMap<>();
        map.put("productCode", productCode);
        map.put("count", count);
        int resultCount=repoMapper.deduct(map);
        if(resultCount==0){
            throw new ServiceException("庫存不足");
        }
    }
}
