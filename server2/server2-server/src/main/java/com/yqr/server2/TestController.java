package com.yqr.server2;

import com.yqr.common.result.R;
import com.yqr.common.util.ValidateUtils;
import io.seata.core.exception.TransactionException;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.extension.ExtensionLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: springclouddubbotest
 * @description: 测试seata
 * @author: Mr.Qian
 * @create: 2021-04-15 23:42
 **/
@RestController
@Slf4j
public class TestController {
    @Autowired
    private TestService testService;
    @GetMapping("/test")
    public R sayHello(
            @ApiParam(required = true, name = "userId", value = "userId") @RequestParam(value = "userId", defaultValue = "", required = true) String userId
    ){
        if(!ValidateUtils.isLong(userId)){
            return R.fail("用户id必传");
        }
        try {
            testService.testRollback(Long.valueOf(userId));
        } catch (TransactionException e) {
            log.error("seata事务出错了",e);
        }
        return R.success();
    }
}
