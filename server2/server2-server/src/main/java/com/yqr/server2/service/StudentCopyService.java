package com.yqr.server2.service;
import com.yqr.server2.service.StudentCopyService;
import com.yqr.server2.persistence.model.StudentCopy;
import com.yqr.server2.persistence.manager.StudentCopyManager;
import com.sohu.idcenter.IdWorker;
import com.yqr.common.result.PageInfoWrapper;
import com.yqr.common.result.R;
import com.yqr.common.util.ValidateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.math.BigDecimal;
import com.yqr.service.BaseServiceImpl;
import java.util.*;


/**
* 接口实现说明: <>
* @author : Simon
* @date : 2021-05-25 08:56:48
*/
@Service
@Slf4j
public class StudentCopyService{

    @Autowired
    private StudentCopyManager studentCopyManager;
    @Autowired
    private IdWorker idWorker;

    /**
    * 分页查询StudentCopy
    */

    public R<PageInfoWrapper<StudentCopy>> queryStudentCopyForPage(Integer pageNum,Integer pageSize) {
        PageHelper.startPage(null == pageNum ? 1 : pageNum, null == pageSize ? 10 : pageSize);
        List<StudentCopy> list = studentCopyManager.selectAll();
        PageInfo<StudentCopy> pageInfo = new PageInfo<>(list);
        return R.data(PageInfoWrapper.build(pageInfo, list));
    }

    /**
    * 根据主键查询StudentCopy
    */

    public R<StudentCopy> getStudentCopyByPrimaryKey(Long id){
        if(ValidateUtils.isNull(id)){
            return R.fail("查询参数不能为空");
        }
StudentCopy studentCopy = studentCopyManager.selectByPrimaryKey(id);
        if(ValidateUtils.isNull(studentCopy)){
            return R.fail("查询不到要编辑的元素");
        }
        return R.data(studentCopy);
    }

    /**
    * 新增StudentCopy
    */

    public R insertStudentCopy(StudentCopy studentCopy){
        if (ValidateUtils.isNull(studentCopy.getName()) || ValidateUtils.exceedLength(studentCopy.getName(), 20)) {
        return R.fail("name参数长度必须在 1-20 之间!");
        }
        if (ValidateUtils.isNull(studentCopy.getGender()) || ValidateUtils.exceedLength(studentCopy.getGender(), 6)) {
        return R.fail("gender参数长度必须在 1-6 之间!");
        }
        if (ValidateUtils.isNull(studentCopy.getGrade()) || ValidateUtils.exceedLength(studentCopy.getGrade(), 2)) {
        return R.fail("grade参数长度必须在 1-2 之间!");
        }

studentCopy.setId(idWorker.getId());
studentCopyManager.insert(studentCopy);
        return R.success("ok");
    }

    /**
    * 编辑StudentCopy
    */

    public R updateStudentCopy(StudentCopy studentCopy){
        if (ValidateUtils.isNull(studentCopy.getName()) || ValidateUtils.exceedLength(studentCopy.getName(), 20)) {
            return R.fail("name参数长度必须在 1-20 之间!");
        }

        if (ValidateUtils.isNull(studentCopy.getGender()) || ValidateUtils.exceedLength(studentCopy.getGender(), 6)) {
            return R.fail("gender参数长度必须在 1-6 之间!");
        }





        if (ValidateUtils.isNull(studentCopy.getGrade()) || ValidateUtils.exceedLength(studentCopy.getGrade(), 2)) {
            return R.fail("grade参数长度必须在 1-2 之间!");
        }

        //在这边写编辑逻辑

studentCopyManager.updateByPrimaryKeySelective(studentCopy);
        return R.success("ok");
    }

}
