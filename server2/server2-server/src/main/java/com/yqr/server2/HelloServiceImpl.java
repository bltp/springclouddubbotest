package com.yqr.server2;

import com.alibaba.dubbo.config.annotation.Service;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

@Service
public class HelloServiceImpl implements IHelloService {
    @Value("${dubbo.application.name}")
    private String serviceName;
    
    @Override
    public String sayHello(String name) {
        
        
        return String.format("[%s]:Hello,%s,%s",serviceName,name,"test");
    }
}
