package com.yqr.server2.dubbo;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.yqr.server2.OrderService;
import com.yqr.server2.persistence.model.Order;
import com.yqr.service.BaseServiceImpl;

/**
 * @program: springclouddubbotest
 * @description: 订单服务
 * @author: Mr.Qian
 * @create: 2021-04-15 23:16
 **/
@Service
@DS("order")
public class OrderServiceImpl extends BaseServiceImpl<Order> implements OrderService {

}
