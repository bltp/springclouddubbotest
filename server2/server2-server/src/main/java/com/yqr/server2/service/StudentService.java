package com.yqr.server2.service;
import com.yqr.server2.service.StudentService;
import com.yqr.server2.persistence.model.Student;
import com.yqr.server2.persistence.manager.StudentManager;
import com.sohu.idcenter.IdWorker;
import com.yqr.common.result.PageInfoWrapper;
import com.yqr.common.result.R;
import com.yqr.common.util.ValidateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.math.BigDecimal;
import com.yqr.service.BaseServiceImpl;
import java.util.*;


/**
* 接口实现说明: <>
* @author : Simon
* @date : 2021-05-25 08:57:27
*/
@Service
@Slf4j
public class StudentService{

    @Autowired
    private StudentManager studentManager;
    @Autowired
    private IdWorker idWorker;

    /**
    * 分页查询Student
    */

    public R<PageInfoWrapper<Student>> queryStudentForPage(Integer pageNum,Integer pageSize) {
        PageHelper.startPage(null == pageNum ? 1 : pageNum, null == pageSize ? 10 : pageSize);
        List<Student> list = studentManager.selectAll();
        PageInfo<Student> pageInfo = new PageInfo<>(list);
        return R.data(PageInfoWrapper.build(pageInfo, list));
    }

    /**
    * 根据主键查询Student
    */

    public R<Student> getStudentByPrimaryKey(Long id){
        if(ValidateUtils.isNull(id)){
            return R.fail("查询参数不能为空");
        }
Student student = studentManager.selectByPrimaryKey(id);
        if(ValidateUtils.isNull(student)){
            return R.fail("查询不到要编辑的元素");
        }
        return R.data(student);
    }

    /**
    * 新增Student
    */

    public R insertStudent(Student student){
        if (ValidateUtils.isNull(student.getName()) || ValidateUtils.exceedLength(student.getName(), 20)) {
        return R.fail("name参数长度必须在 1-20 之间!");
        }
        if (ValidateUtils.isNull(student.getGender()) || ValidateUtils.exceedLength(student.getGender(), 6)) {
        return R.fail("gender参数长度必须在 1-6 之间!");
        }
        if (ValidateUtils.isNull(student.getGrade()) || ValidateUtils.exceedLength(student.getGrade(), 2)) {
        return R.fail("grade参数长度必须在 1-2 之间!");
        }

student.setId(idWorker.getId());
studentManager.insert(student);
        return R.success("ok");
    }

    /**
    * 编辑Student
    */

    public R updateStudent(Student student){
        if (ValidateUtils.isNull(student.getName()) || ValidateUtils.exceedLength(student.getName(), 20)) {
            return R.fail("name参数长度必须在 1-20 之间!");
        }

        if (ValidateUtils.isNull(student.getGender()) || ValidateUtils.exceedLength(student.getGender(), 6)) {
            return R.fail("gender参数长度必须在 1-6 之间!");
        }





        if (ValidateUtils.isNull(student.getGrade()) || ValidateUtils.exceedLength(student.getGrade(), 2)) {
            return R.fail("grade参数长度必须在 1-2 之间!");
        }

        //在这边写编辑逻辑

studentManager.updateByPrimaryKeySelective(student);
        return R.success("ok");
    }

}
