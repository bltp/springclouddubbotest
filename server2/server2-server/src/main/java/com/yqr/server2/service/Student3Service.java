package com.yqr.server2.service;
import com.yqr.server2.service.Student3Service;
import com.yqr.server2.persistence.model.Student3;
import com.yqr.server2.persistence.manager.Student3Manager;
import com.sohu.idcenter.IdWorker;
import com.yqr.common.result.PageInfoWrapper;
import com.yqr.common.result.R;
import com.yqr.common.util.ValidateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.math.BigDecimal;
import com.yqr.service.BaseServiceImpl;
import java.util.*;


/**
* 接口实现说明: <>
* @author : Simon
* @date : 2021-05-25 09:31:33
*/
@Service
@Slf4j
public class Student3Service{

    @Autowired
    private Student3Manager student3Manager;
    @Autowired
    private IdWorker idWorker;

    /**
    * 分页查询Student3
    */

    public R<PageInfoWrapper<Student3>> queryStudent3ForPage(Integer pageNum,Integer pageSize) {
        PageHelper.startPage(null == pageNum ? 1 : pageNum, null == pageSize ? 10 : pageSize);
        List<Student3> list = student3Manager.selectAll();
        PageInfo<Student3> pageInfo = new PageInfo<>(list);
        return R.data(PageInfoWrapper.build(pageInfo, list));
    }

    /**
    * 根据主键查询Student3
    */

    public R<Student3> getStudent3ByPrimaryKey(Long id){
        if(ValidateUtils.isNull(id)){
            return R.fail("查询参数不能为空");
        }
        Student3 student3 = student3Manager.selectByPrimaryKey(id);
        if(ValidateUtils.isNull(student3)){
            return R.fail("查询不到要编辑的元素");
        }
        return R.data(student3);
    }

    /**
    * 新增Student3
    */

    public R insertStudent3(Student3 student3){
        if (ValidateUtils.isNull(student3.getName()) || ValidateUtils.exceedLength(student3.getName(), 20)) {
        return R.fail("name参数长度必须在 1-20 之间!");
        }
        if (ValidateUtils.isNull(student3.getGender()) || ValidateUtils.exceedLength(student3.getGender(), 6)) {
        return R.fail("gender参数长度必须在 1-6 之间!");
        }
        if (ValidateUtils.isNull(student3.getGrade()) || ValidateUtils.exceedLength(student3.getGrade(), 2)) {
        return R.fail("grade参数长度必须在 1-2 之间!");
        }

        student3.setId(idWorker.getId());
        student3Manager.insert(student3);
        return R.success("ok");
    }

    /**
    * 编辑Student3
    */

    public R updateStudent3(Student3 student3){
        if (ValidateUtils.isNull(student3.getName()) || ValidateUtils.exceedLength(student3.getName(), 20)) {
            return R.fail("name参数长度必须在 1-20 之间!");
        }

        if (ValidateUtils.isNull(student3.getGender()) || ValidateUtils.exceedLength(student3.getGender(), 6)) {
            return R.fail("gender参数长度必须在 1-6 之间!");
        }





        if (ValidateUtils.isNull(student3.getGrade()) || ValidateUtils.exceedLength(student3.getGrade(), 2)) {
            return R.fail("grade参数长度必须在 1-2 之间!");
        }

        //在这边写编辑逻辑

        student3Manager.updateByPrimaryKeySelective(student3);
            return R.success("ok");
        }

}
